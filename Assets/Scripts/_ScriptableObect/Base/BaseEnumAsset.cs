﻿using UnityEngine;

[CreateAssetMenu(fileName = "Enum", menuName = "RealEstateMaster/Enums/Enum", order = -1)]
public class BaseEnumAsset : ScriptableObject
{
    #region Public Varaibles

    public string nameOfEnum;
    public Sprite iconForEnum;

    #endregion
}
