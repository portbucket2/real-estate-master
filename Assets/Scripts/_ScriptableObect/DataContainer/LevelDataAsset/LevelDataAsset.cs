﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using com.faith.core;

[CreateAssetMenu(fileName = "LevelDataAsset", menuName = "RealEstateMaster/Asset Container/LevelDataAsset")]
public class LevelDataAsset : ScriptableObject
{
    #region Custom Variables

    [System.Serializable]
    public class LevelData
    {
        [Range(1, 10)]
        public int numberOfSellerLandAgent;
        [Range(1, 10)]
        public int numberOfBuyerLandAgent;
    }

    #endregion

    #region Public Variables

#if UNITY_EDITOR

    public bool showSharedData;
    public bool overrideExistingData;

#endif

    [Range(1, 1000)]
    public int maxDay = 1;

    [Range(2, 10)]
    public int maxNumberOfCustomerPerDay = 1;
    [Range(0.1f, 1f)]
    public float percentageOfBuyerPerDay = 0.1f;
    public AnimationCurve curveForBuyerPercentage;

    public SharedData sharedGameData;

    public List<LevelData> listOfLevelData;



    #endregion

    #region Private Varaibles

    private bool _isDataLoaded;
    private SavedData<int> _dayIndex;
    private SavedData<int> _levelIndex;
    private UnityAction OnLevelDataLoaded;

    #endregion

    #region Configuretion

    private void OnDayIndexLoaded(int _loadedDayIndex) {

        sharedGameData.dayIndex = _loadedDayIndex;
        if (!_isDataLoaded)
        {
            _isDataLoaded = true;
            OnLevelDataLoaded.Invoke();
        }
    }

    private void OnLevelIndexLoaded(int _loadedLevelIndex)
    {
        sharedGameData.levelIndex = _loadedLevelIndex;
        if (!_isDataLoaded)
        {
            _isDataLoaded = true;
            OnLevelDataLoaded.Invoke();
        }

    }

    private bool IsValidLevelIndex(int levelIndex) {

        if (levelIndex >= 0 && levelIndex < maxNumberOfCustomerPerDay)
            return true;

        return false;
    }

    private bool IsValidDayIndex(int dayIndex) {

        if (dayIndex >= 0 && dayIndex < maxDay) {

            return true;
        }

        return false;
    }

    #endregion

    #region Public Callback

#if UNITY_EDITOR

    public void GenerateNumberOfLandAgentBothSellerAndBuyer() {

        if (!overrideExistingData)
        {
            int currentNumberOfLevelData = listOfLevelData.Count;
            if (currentNumberOfLevelData >= maxDay)
            {
                int numberOfItemToBeRemoved = currentNumberOfLevelData - maxDay;
                for (int i = 0; i < numberOfItemToBeRemoved; i++) {

                    listOfLevelData.RemoveAt(listOfLevelData.Count - 1);
                }
            }
            else
            {
                for (int i = 0; i < maxDay; i++)
                {
                    if (i < currentNumberOfLevelData)
                    {
                        listOfLevelData[i].numberOfBuyerLandAgent = Mathf.CeilToInt(maxNumberOfCustomerPerDay * percentageOfBuyerPerDay * curveForBuyerPercentage.Evaluate((float)(i + 1) / maxDay));
                        listOfLevelData[i].numberOfSellerLandAgent = maxNumberOfCustomerPerDay - listOfLevelData[i].numberOfBuyerLandAgent;
                    }
                    else {

                        LevelData newLevelData = new LevelData();
                        newLevelData.numberOfBuyerLandAgent = Mathf.CeilToInt(maxNumberOfCustomerPerDay * percentageOfBuyerPerDay * curveForBuyerPercentage.Evaluate((float)(i + 1) / maxDay));
                        newLevelData.numberOfSellerLandAgent = maxNumberOfCustomerPerDay - newLevelData.numberOfBuyerLandAgent;
                        listOfLevelData.Add(newLevelData);
                    }
                }
            }
        }
        else {

            listOfLevelData = new List<LevelData>();
            for (int i = 0; i < maxDay; i++)
            {

                LevelData newLevelData = new LevelData();
                newLevelData.numberOfBuyerLandAgent = Mathf.CeilToInt(maxNumberOfCustomerPerDay * percentageOfBuyerPerDay * curveForBuyerPercentage.Evaluate((float)(i + 1) / maxDay));
                newLevelData.numberOfSellerLandAgent = maxNumberOfCustomerPerDay - newLevelData.numberOfBuyerLandAgent;
                listOfLevelData.Add(newLevelData);
            }
        }
    }

#endif

    public void Initialization(UnityAction OnLevelDataLoaded) {

        _isDataLoaded = false;
        this.OnLevelDataLoaded = OnLevelDataLoaded;

        sharedGameData = new SharedData();
        _dayIndex = new SavedData<int>("DAY_INDEX", 0, OnDayIndexLoaded);
        _levelIndex = new SavedData<int>("LEVEL_INDEX", 0, OnLevelIndexLoaded);

    }


    public void GoToNextLevel() {

        _levelIndex.SetData(_levelIndex.GetData() + 1);
    }

    public void GotToNextDay() {

        int nextDay = _dayIndex.GetData() + 1;
        if (IsValidDayIndex(nextDay))
            _dayIndex.SetData(nextDay);
        else
        {
            _dayIndex.SetData(0);
            _levelIndex.SetData(0);
        }
    }

    #endregion
}
