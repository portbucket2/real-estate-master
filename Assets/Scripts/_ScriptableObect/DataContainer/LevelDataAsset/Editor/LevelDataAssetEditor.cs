﻿using UnityEngine;
using UnityEditor;
using com.faith.core;

[CustomEditor(typeof(LevelDataAsset))]
public class LevelDataAssetEditor : BaseEditorClass
{
    #region Private Variables

    private LevelDataAsset _reference;

    private SerializedProperty _sp_maxLevel;

    private SerializedProperty _sp_maxNumberOfCustomerPerDay;
    private SerializedProperty _sp_percentageOfBuyerPerDay;
    private SerializedProperty _sp_curveForBuyerPercentage;

    private SerializedProperty _sp_sharedGameData;

    private SerializedProperty _sp_listOfLevelData;

    

    #endregion

    #region Editor

    public override void OnEnable()
    {
        _reference = (LevelDataAsset)target;

        if (_reference == null)
            return;

        _sp_maxLevel = serializedObject.FindProperty("maxDay");

        _sp_maxNumberOfCustomerPerDay = serializedObject.FindProperty("maxNumberOfCustomerPerDay");
        _sp_percentageOfBuyerPerDay = serializedObject.FindProperty("percentageOfBuyerPerDay");
        _sp_curveForBuyerPercentage = serializedObject.FindProperty("curveForBuyerPercentage");

        _sp_sharedGameData = serializedObject.FindProperty("sharedGameData");

        _sp_listOfLevelData = serializedObject.FindProperty("listOfLevelData");

        
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_sp_maxLevel);

        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(_sp_maxNumberOfCustomerPerDay);
        EditorGUILayout.PropertyField(_sp_percentageOfBuyerPerDay);
        EditorGUILayout.PropertyField(_sp_curveForBuyerPercentage);

        EditorGUILayout.BeginHorizontal();
        {
            _reference.overrideExistingData = EditorGUILayout.Toggle(
                "Override",
                _reference.overrideExistingData
            );
            if (GUILayout.Button("GenerateData")) {

                _reference.GenerateNumberOfLandAgentBothSellerAndBuyer();
            }
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        _reference.showSharedData = EditorGUILayout.Foldout(
                _reference.showSharedData,
                "SharedData",
                true
            );
        if (_reference.showSharedData) {

            EditorGUI.indentLevel += 1;

            EditorGUILayout.PropertyField(_sp_sharedGameData);

            EditorGUI.indentLevel -= 1;

            EditorGUILayout.Space();
        }

        DrawHorizontalLine();
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(_sp_listOfLevelData);

        serializedObject.ApplyModifiedProperties();
    }

    #endregion
}
