﻿using UnityEngine;
using com.faith.core;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "LandAssets", menuName = "RealEstateMaster/Asset Container/LandAssets")]
public class LandAssets : ScriptableObject
{
    #region Custom Variables


    [System.Serializable]
    private class LandTypeAvailableOnLevel
    {
        public int              requiredLevel;
        public BaseEnumAsset    landType;
    }


    #endregion

    #region Public Variables

    public FloatReference           landValueIncreaseAfterDecoration;
    public List<EnumAssetForLand>   listOfLandAssetInfo;

    #endregion

    #region Private Variables


    [SerializeField] private List<LandTypeAvailableOnLevel> _listOfLandTypeAvailableOnLevel;

    #endregion

    #region SciptableObject


    public void OnValidate()
    {
        EditorInitialization();
    }

    #endregion

    #region Configuretion

    private void EditorInitialization() {

#if UNITY_EDITOR


        _listOfLandTypeAvailableOnLevel = new List<LandTypeAvailableOnLevel>();

        int numberOfItemInList = listOfLandAssetInfo.Count;
        for (int i = 0; i < numberOfItemInList; i++)
        {

            if (listOfLandAssetInfo[i] != null)
            {
                EnumAssetForLand currentLand = listOfLandAssetInfo[i];
                currentLand.landIndex = i;

                int numberOfAvailableLandType = _listOfLandTypeAvailableOnLevel.Count;
                bool isUniqueLandType = true;
                for (int j = 0; j < numberOfAvailableLandType; j++)
                {

                    LandTypeAvailableOnLevel currentLandType = _listOfLandTypeAvailableOnLevel[j];
                    if (currentLandType.landType == currentLand.landType)
                    {

                        currentLandType.requiredLevel = Mathf.Min(currentLandType.requiredLevel, currentLand.validFromLevel);
                        isUniqueLandType = false;
                    }
                }

                if (isUniqueLandType)
                {

                    _listOfLandTypeAvailableOnLevel.Add(new LandTypeAvailableOnLevel()
                    {
                        requiredLevel = currentLand.validFromLevel,
                        landType = currentLand.landType
                    });
                }
            }
        }


        foreach (LandTypeAvailableOnLevel landTypeAvailable in _listOfLandTypeAvailableOnLevel)
        {

            CoreDebugger.Debug.Log("LandType : " + landTypeAvailable.landType.name + ", At Level : " + landTypeAvailable.requiredLevel);
        }
#endif
    }

    #endregion

    #region Public Callback

    public EnumAssetForLand GetALandAssetInfo(int level = 0) {

        List<EnumAssetForLand> selectedLandAssetInfo = new List<EnumAssetForLand>();
        foreach (EnumAssetForLand landAssetInfo in listOfLandAssetInfo) {

            if (level >= landAssetInfo.validFromLevel)
                selectedLandAssetInfo.Add(landAssetInfo);
        }
        
        return selectedLandAssetInfo[Random.Range(0, selectedLandAssetInfo.Count)];
    }

    public BaseEnumAsset GetLandAssetTypeForBuyer(int level) {
        
        int numberOfAvailableLandTypeForBuyer = 0;
        List<BaseEnumAsset> listOfLandTypeAvailableForBuyer = new List<BaseEnumAsset>();

        foreach (LandTypeAvailableOnLevel landTypeAvailable in _listOfLandTypeAvailableOnLevel) {

            if (level >= landTypeAvailable.requiredLevel)
            {
                listOfLandTypeAvailableForBuyer.Add(landTypeAvailable.landType);
                numberOfAvailableLandTypeForBuyer++;
            }
        }

        return listOfLandTypeAvailableForBuyer[Random.Range(0, numberOfAvailableLandTypeForBuyer)];
    }

    #endregion

}
