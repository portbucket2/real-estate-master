﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using com.faith.core;

[CreateAssetMenu(fileName = "Inventory", menuName = "RealEstateMaster/Inventory")]
public class Inventory : ScriptableObject
{
    #region Custom Variables

    private class InventoryLandInfo
    {
        public SavedData<int> landUniqueID;

        public SavedData<int>       houseTypeIndex;
        public SavedData<int>       houseIndex;

        public SavedData<bool> isHouseDecorated;
    }

    private class DisplayTableInInventory
    {

        #region Private Variables

        private int _stackIndex;
        private int _maxStackSize = -1;
        private SavedData<int> _numberOfItemInStack;
        private List<InventoryLandInfo> _listOfItemInDisplayTable;
        
        #endregion

        #region Configuretion

        private void UpdateStackInfo(int numberOfItem)
        {
            _listOfItemInDisplayTable = new List<InventoryLandInfo>();
            for (int i = 0; i < numberOfItem; i++)
            {
                _listOfItemInDisplayTable.Add(new InventoryLandInfo());
                _listOfItemInDisplayTable[i].landUniqueID        = new SavedData<int>("Inventory_Stack(" + _stackIndex + ")_ItemIndex(" + i + ")_LandUniqueID", 0);

                _listOfItemInDisplayTable[i].houseTypeIndex      = new SavedData<int>("Inventory_Stack(" + _stackIndex + ")_ItemIndex(" + i + ")_HouseTypeIndex", -1);
                _listOfItemInDisplayTable[i].houseIndex          = new SavedData<int>("Inventory_Stack(" + _stackIndex + ")_ItemIndex(" + i + ")_HouseIndex", -1);

                _listOfItemInDisplayTable[i].isHouseDecorated    = new SavedData<bool>("Inventory_Stack(" + _stackIndex + ")_ItemIndex(" + i + ")_IsHouseDecorated", false);

            }
        }

        private bool IsValidItemIndex(int itemIndex)
        {

            if (itemIndex >= 0 && itemIndex < NumberOfItemInDisplayTable)
                return true;

            CoreDebugger.Debug.LogError("Invalid ItemIndex = " + itemIndex + ", in Stack(" + _stackIndex + ")");
            return false;
        }

        #endregion

        #region Public Callback

        public DisplayTableInInventory(int stackIndex, int maxStackSize = -1)
        {
            _maxStackSize = maxStackSize;

            _stackIndex = stackIndex;
            _numberOfItemInStack = new SavedData<int>("Inventory_NumberOfItemInStack(" + _stackIndex + ")", 0, UpdateStackInfo);
        }

        public bool IsEmptySlotAvailable() {

            return (_maxStackSize == -1 || _numberOfItemInStack.GetData() < _maxStackSize) ? true : false;
        }

        public int NumberOfEmptySlotAvailable()
        {
            return _maxStackSize == -1 ? 1 : (_maxStackSize - _numberOfItemInStack.GetData());
        }

        public int NumberOfItemInDisplayTable { get { return _numberOfItemInStack.GetData(); } }

        public int GetLandUniqueID(int displayItemIndex)
        {
            if (IsValidItemIndex(displayItemIndex)) {

                return _listOfItemInDisplayTable[displayItemIndex].landUniqueID.GetData();
            }

            return -1;
        }

        public int GetHouseTypeIndex(int displayItemIndex)
        {

            if (IsValidItemIndex(displayItemIndex))
            {
                return _listOfItemInDisplayTable[displayItemIndex].houseTypeIndex.GetData();
            }

            return -1;
        }

        public int GetHouseIndex(int displayItemIndex)
        {
            if (IsValidItemIndex(displayItemIndex))
            {
                return _listOfItemInDisplayTable[displayItemIndex].houseIndex.GetData();
            }
            return -1;
        }

        public bool IsHouseDecorated(int displayItemIndex)
        {

            if (IsValidItemIndex(displayItemIndex))
            {
                return _listOfItemInDisplayTable[displayItemIndex].isHouseDecorated.GetData();
            }

            return false;
        }

        public int AddItemToStack(int itemUniqueID)
        {
            if (IsEmptySlotAvailable())
            {
                int newValue = _numberOfItemInStack.GetData();
                _numberOfItemInStack.SetData(newValue + 1);
                _listOfItemInDisplayTable[newValue].landUniqueID.SetData(itemUniqueID);
                _listOfItemInDisplayTable[newValue].isHouseDecorated.SetData(false);
                return newValue;
            }

            return -1;
        }

        public bool AddHouseInfo(int itemIndex, int houseTypeIndex, int houseIndex) {

            if (IsValidItemIndex(itemIndex)) {

                _listOfItemInDisplayTable[itemIndex].houseTypeIndex.SetData(houseTypeIndex);
                _listOfItemInDisplayTable[itemIndex].houseIndex.SetData(houseIndex);
                return true;
            }

            return false;
            
        }

        public void SetHouseAsDecorated(int itemIndex, bool houseDecorated) {

            if (IsValidItemIndex(itemIndex)) {

                _listOfItemInDisplayTable[itemIndex].isHouseDecorated.SetData(houseDecorated);
            }
        }

        public bool RemoveItemByItemIndex(int itemIndex) {

            if (IsValidItemIndex(itemIndex))
            {
                int currentNumberOfItemInStack = _numberOfItemInStack.GetData();
                for (int i = itemIndex; i < currentNumberOfItemInStack - 1; i++)
                {
                    _listOfItemInDisplayTable[i].landUniqueID.SetData(_listOfItemInDisplayTable[i + 1].landUniqueID.GetData());
                    _listOfItemInDisplayTable[i].houseTypeIndex.SetData(_listOfItemInDisplayTable[i + 1].houseTypeIndex.GetData());
                    _listOfItemInDisplayTable[i].houseIndex.SetData(_listOfItemInDisplayTable[i + 1].houseIndex.GetData());
                    _listOfItemInDisplayTable[i].isHouseDecorated.SetData(_listOfItemInDisplayTable[i + 1].isHouseDecorated.GetData());
                }

                _listOfItemInDisplayTable.RemoveAt(_listOfItemInDisplayTable.Count - 1);
                _listOfItemInDisplayTable.TrimExcess();

                _numberOfItemInStack.SetData(_numberOfItemInStack.GetData() - 1);

                return true;
            }

            return false;
        }

        public bool RemoveItemByItemUniqueID(int itemUniqueID) {

            int currentNumberOfItemInStack = _numberOfItemInStack.GetData();
            int deleteIndex = -1;
            for (int i = 0; i < currentNumberOfItemInStack; i++) {

                if (_listOfItemInDisplayTable[i].landUniqueID.GetData() == itemUniqueID) {

                    deleteIndex = i;
                    break;
                }
            }

            if (deleteIndex != -1) {

                return RemoveItemByItemIndex(deleteIndex);
            }

            return false;
        }

        #endregion

    }

    #endregion

    #region Public Variables

    public int NumberOfAvailableInventoryStack
    {
        get { return _numberOfInventoryStack.GetData(); }
    }

    public int NumberOfItemSlotPerDisplayTable { get { return _numberOfSlotInPerStack; } }
    

    #endregion

    #region Private Variables

    [SerializeField] private IntReference _numberOfSlotInPerStack;
    [SerializeField, Range(500, 1000)] private int _basePriceForInventoryStack = 500;
    [SerializeField, Range(0f, 0.5f)] private float _priceIncreasePerStack = 0.1f;

    private List<DisplayTableInInventory> _listOfDisplayTableInInventory;

    private SavedData<int> _numberOfInventoryStack;

    #endregion

    #region Configuretion

    private bool IsValidIndexForDisplayTableInInventory(int displayTableIndex) {

        if (displayTableIndex >= 0 && displayTableIndex < _numberOfInventoryStack.GetData())
            return true;

        CoreDebugger.Debug.LogError("Invalid DisplayTableIndex in 'Inventory' : " + displayTableIndex);
        return false;
    }

    private void FillEmptySlot() {

        
        int numberOfInventoryStack = _numberOfInventoryStack.GetData();
        for (int i = 0; i < numberOfInventoryStack - 1; i++)
        {
            if (_listOfDisplayTableInInventory[i].NumberOfEmptySlotAvailable() > 0) {

                for (int j = i + 1; j < numberOfInventoryStack; j++)
                {

                    if (_listOfDisplayTableInInventory[j].NumberOfItemInDisplayTable > 0)
                    {
                        
                        AddLandInInventory(GetLandUniqueID(j, 0), (displayTableIndex, displayItemIndex) =>
                        {
                            AddHouseIndex(displayTableIndex, displayItemIndex, GetHouseTypeIndex(j, 0), GetHouseIndex(j, 0));
                            SetHouseAsDecorated(displayTableIndex, displayItemIndex, IsHouseDecorated(j, 0));
                            _listOfDisplayTableInInventory[j].RemoveItemByItemIndex(0);
                        });
                    }
                }
            }
        }
    }

    #endregion

    #region Public Callback

    public void Initialization() {

        _numberOfInventoryStack = new SavedData<int>("Inventory_NumberOfStack", 1);
        _listOfDisplayTableInInventory = new List<DisplayTableInInventory>();
        for (int i = 0; i < _numberOfInventoryStack.GetData() ; i++) {

            _listOfDisplayTableInInventory.Add(new DisplayTableInInventory(i, _numberOfSlotInPerStack));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>-1 means no empty slot available, other value indicate the index of stack that has empty slot</returns>
    public int IsEmptySlotAvailable() {

        for (int i = 0; i < _numberOfInventoryStack.GetData(); i++)
        {
            if (_listOfDisplayTableInInventory[i].IsEmptySlotAvailable())
                return i;
        }

        return -1;
    }

    public int NumberOfEmptySlotAvailable(int displayTableIndex) {

        if (IsValidIndexForDisplayTableInInventory(displayTableIndex))
        {
            return _listOfDisplayTableInInventory[displayTableIndex].NumberOfEmptySlotAvailable();
        }

        return -1;
    }

    public int NumberOfItemInDisplayTable(int displayTableIndex) {

        if (IsValidIndexForDisplayTableInInventory(displayTableIndex))
        {
            return _listOfDisplayTableInInventory[displayTableIndex].NumberOfItemInDisplayTable;
        }

        return -1;
    }

    public int GetLandUniqueID(int displayTableIndex, int displayItemIndex)
    {

        if (IsValidIndexForDisplayTableInInventory(displayTableIndex))
        {
            return _listOfDisplayTableInInventory[displayTableIndex].GetLandUniqueID(displayItemIndex);
        }

        return -1;
    }

    public int GetHouseTypeIndex(int displayTableIndex, int displayItemIndex) {

        if (IsValidIndexForDisplayTableInInventory(displayTableIndex)) {
            return _listOfDisplayTableInInventory[displayTableIndex].GetHouseTypeIndex(displayItemIndex);
        }

        return -1;
    }

    public int GetHouseIndex(int displayTableIndex, int displayItemIndex) {
        if (IsValidIndexForDisplayTableInInventory(displayTableIndex))
        {
            return _listOfDisplayTableInInventory[displayTableIndex].GetHouseIndex(displayItemIndex);
        }
        return -1;
    }


    public bool IsHouseDecorated(int displayTableIndex, int displayItemIndex) {

        if (IsValidIndexForDisplayTableInInventory(displayTableIndex))
        {
            return _listOfDisplayTableInInventory[displayTableIndex].IsHouseDecorated(displayItemIndex);
        }

        return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="landUniqeID"></param>
    /// <returns>true : if adding was successful</returns>
    public bool AddLandInInventory(int landUniqeID, UnityAction<int, int> OnPassiedStackAndItemIndex = null) {

        int stackIndex = IsEmptySlotAvailable();
        if (stackIndex != -1) {

            int itemIndexOnStack = _listOfDisplayTableInInventory[stackIndex].AddItemToStack(landUniqeID);
            if (itemIndexOnStack != -1) {
                OnPassiedStackAndItemIndex?.Invoke(stackIndex, itemIndexOnStack);
                return true;
            }
        }
        
        return false;
    }

    public bool AddHouseIndex(int displayTableIndex, int displayItemIndex, int houseTypeIndex,int houseIndex) {

        if (IsValidIndexForDisplayTableInInventory(displayTableIndex)) {

            return _listOfDisplayTableInInventory[displayTableIndex].AddHouseInfo(displayItemIndex, houseTypeIndex, houseIndex);
        }

        return false;
    }

    public void SetHouseAsDecorated(int displayTableIndex, int diplayItemIndex, bool houseDecorated) {

        if (IsValidIndexForDisplayTableInInventory(displayTableIndex)) {

            _listOfDisplayTableInInventory[displayTableIndex].SetHouseAsDecorated(diplayItemIndex, houseDecorated);
        }
    }

    public bool RemoveItemFromInventory(int itemUniqueID) {

        foreach (DisplayTableInInventory inventoryStack in _listOfDisplayTableInInventory) {

            if (inventoryStack.RemoveItemByItemUniqueID(itemUniqueID))
                return true;
        }

        return false;
    }

    public bool RemoveItemFromInventory(int displayTableIndex, int displayItemIndex) {

        if (IsValidIndexForDisplayTableInInventory(displayTableIndex)) {

            if (_listOfDisplayTableInInventory[displayTableIndex].RemoveItemByItemIndex(displayItemIndex)) {
                FillEmptySlot();
                return true;
            }
        }

        return false;
    }

    public int GetPurchasePriceForBuyingInventoryStack() {

        return (int) (_basePriceForInventoryStack + (_numberOfInventoryStack.GetData() * (_basePriceForInventoryStack * _priceIncreasePerStack)));
    }


    public void PurchaseDisplayTableForInventory() {

        int currentNumberOfInventoryStack = _numberOfInventoryStack.GetData();
        _numberOfInventoryStack.SetData(currentNumberOfInventoryStack + 1);
        _listOfDisplayTableInInventory.Add(new DisplayTableInInventory(currentNumberOfInventoryStack, _numberOfSlotInPerStack));
    }

    #endregion

}
