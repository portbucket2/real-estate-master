﻿using UnityEngine;
using UnityEditor;
using com.faith.core;

[CustomEditor(typeof(Inventory))]
public class InventoryEditor : BaseEditorClass
{
    #region Private Variables

    private Inventory _reference;

    private SerializedProperty _sp_numberOfSlotInPerStack;
    private SerializedProperty _sp_basePriceForInventoryStack;
    private SerializedProperty _sp_priceIncreasePerStack;

    private int _itemUniqueIDForEntry;
    private int _itemUniqueIDForRemove;

    #endregion

    #region Editor

    public override void OnEnable()
    {
        _reference = (Inventory)target;

        if (_reference == null)
            return;

        _sp_numberOfSlotInPerStack = serializedObject.FindProperty("_numberOfSlotInPerStack");
        _sp_basePriceForInventoryStack = serializedObject.FindProperty("_basePriceForInventoryStack");
        _sp_priceIncreasePerStack = serializedObject.FindProperty("_priceIncreasePerStack");
        
    }

    public override void OnInspectorGUI()
    {
        ShowScriptReference();

        serializedObject.Update();

        EditorGUILayout.PropertyField(_sp_numberOfSlotInPerStack);
        EditorGUILayout.PropertyField(_sp_basePriceForInventoryStack);
        EditorGUILayout.PropertyField(_sp_priceIncreasePerStack);
        
        if (EditorApplication.isPlaying) {

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("PurchasePrice : " + _reference.GetPurchasePriceForBuyingInventoryStack());
                if (GUILayout.Button("Purchase", GUILayout.Width(100)))
                {
                    _reference.PurchaseDisplayTableForInventory();
                }
            }
            EditorGUILayout.EndHorizontal();

            if (_reference.NumberOfAvailableInventoryStack > 0)
            {

                EditorGUILayout.Space();
                DrawHorizontalLine();
                for (int i = 0; i < _reference.NumberOfAvailableInventoryStack; i++)
                {

                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField("InventoryStack(" + i + ")", EditorStyles.boldLabel);
                        EditorGUILayout.LabelField("Status(" + _reference.NumberOfItemInDisplayTable(i) + "/" + _reference.NumberOfItemSlotPerDisplayTable + ")");
                    }
                    EditorGUILayout.EndHorizontal();

                    EditorGUI.indentLevel += 1;

                    for (int j = 0; j < _reference.NumberOfItemInDisplayTable(i); j++) {

                        EditorGUILayout.BeginHorizontal();
                        {
                            EditorGUILayout.LabelField("LandUniqueID : " + _reference.GetLandUniqueID(i,j));
                            if (GUILayout.Button("Remove",GUILayout.Width(100))) {
                                _reference.RemoveItemFromInventory(i,j);
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUI.indentLevel += 1;
                        EditorGUILayout.LabelField("HouseTypeIndex : " + _reference.GetHouseTypeIndex(i,j));
                        EditorGUILayout.LabelField("HouseIndex : " + _reference.GetHouseIndex(i,j));
                        EditorGUILayout.LabelField("IsHouseDecorated : " + _reference.IsHouseDecorated(i,j));
                        EditorGUI.indentLevel -= 1;
                        
                    }

                    EditorGUI.indentLevel -= 1;
                }

                DrawHorizontalLine();
                EditorGUILayout.Space();
                EditorGUILayout.BeginHorizontal();
                {
                    _itemUniqueIDForEntry = EditorGUILayout.IntField(
                            "LandUniqueID",
                            _itemUniqueIDForEntry
                        );
                    if (GUILayout.Button("AddLand", GUILayout.Width(100))) {
                        _reference.AddLandInInventory(_itemUniqueIDForEntry);
                    }
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();
                EditorGUILayout.BeginHorizontal();
                {
                    _itemUniqueIDForRemove = EditorGUILayout.IntField(
                            "LandUniqueID",
                            _itemUniqueIDForRemove
                        );
                    if (GUILayout.Button("RemoveLand", GUILayout.Width(100)))
                    {
                        _reference.RemoveItemFromInventory(_itemUniqueIDForRemove);
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
        }
        


        serializedObject.ApplyModifiedProperties();
    }

    #endregion
}
