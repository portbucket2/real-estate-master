﻿using UnityEngine;
using UnityEngine.AI;
[CreateAssetMenu(fileName = "NavMeshAgentSharedAttributeAsset", menuName = "RealEstateMaster/NavMesh/NavMeshAgentSharedAttributeAsset")]
public class NavMeshAgentSharedAttributeAsset : ScriptableObject
{

    #region Public Variables

    public float speed = 3.5f;
    public float angulerSpeed = 120;
    public float acceleration = 8;

    [Space(5.0f)]
    public float stopingDistance = 0;

    #endregion

    #region Public Callback

    public void AssignValue(ref NavMeshAgent navMeshAgent) {

        navMeshAgent.speed = speed;
        navMeshAgent.angularSpeed = angulerSpeed;
        navMeshAgent.acceleration = acceleration;
        navMeshAgent.stoppingDistance = stopingDistance;
    }

    #endregion
}
