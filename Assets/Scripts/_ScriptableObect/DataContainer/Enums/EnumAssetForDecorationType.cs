﻿using UnityEngine;
using com.faith.core;
[CreateAssetMenu(fileName = "EnumForDecorationType", menuName = "RealEstateMaster/Enums/EnumForDecorationType", order = 2)]
public class EnumAssetForDecorationType : BaseEnumAsset
{
    #region Custom Variables

    [System.Serializable]
    public class DecorationItem
    {
        public int          validFromLevel = 0;
        public string       nameOfDecorationItem;
        public Sprite       iconForDecorationItem;
        public GameObject   decorationPrefab;

        [Space(5.0f)]
        public Material defaultMaterial;
        public Material transaculateMaterial;
    }

    #endregion

    #region Public Variables

    public GameObject       particleForSpawningDecoration;
    [Range(1,10)]
    public float            particleScale;

    [Space(5.0f)]
    public DecorationItem[] decorationItems;

    #endregion

    #region Public Callback

    public void SpawnParticle(ref Transform parentReference) {

        if (particleForSpawningDecoration == null) {

            CoreDebugger.Debug.LogWarning("No Particle was assigned for '" + nameOfEnum + "'");
            return;
        }

        Transform   particleTransform = Instantiate(particleForSpawningDecoration, parentReference).transform;
        Transform[] allTransformReference = particleTransform.GetComponentsInChildren<Transform>(true);

        foreach (Transform transformReference in allTransformReference) {

            transformReference.localScale = Vector3.one * particleScale;
        }

        particleTransform.GetComponent<ParticleSystem>().Play();
    }

    #endregion
}
