﻿using UnityEngine;
using com.faith.core;

[CreateAssetMenu(fileName = "EnumForLandType", menuName = "RealEstateMaster/Enums/EnumForLandType", order = 0)]
public class EnumAssetForLand : ScriptableObject
{
    [HideInInspector,Space(5.0f)]
    public int landIndex;

    public BaseEnumAsset landType;

    [Space(5.0f)]
    public int validFromLevel = 0;

    [Space(5.0f)]
    public string nameOfLand;
    public Sprite iconForLand;

    [Space(5.0f)]
    public GameObject landDisplayPrefab;
    public Material defaultMaterialForLandDisplay;
    public Material transaculateMaterialForLandDisplay;

    [Space(5.0f)]
    public float sellingPrice = 50;
    public float estimatedPrice = 100;

    [Space(5.0f)]
    public SceneReference sceneForTheLand;

    [Space(5.0f)]
    public EnumAssetForDecorationType[] houseTypeCanBePlaced;

    [Space(5.0f)]
    public EnumAssetForDecorationType[] decorationType;
}
