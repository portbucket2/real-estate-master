﻿using UnityEngine;

[CreateAssetMenu(fileName = "PopUpPanelInfo", menuName = "RealEstateMaster/PopUpPanelInfo")]
public class PopUpPanelInfo : ScriptableObject
{
    public Sprite iconForInteractableButton;

    [Space(5.0f)]
    public string headline;
    [Multiline]
    public string message;
    [Multiline]
    public string buttonInfo;
}
