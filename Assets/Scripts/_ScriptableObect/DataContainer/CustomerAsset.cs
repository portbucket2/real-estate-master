﻿using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu(fileName ="CustomerAsset", menuName = "RealEstateMaster/Asset Container/CustomerAsset")]
public class CustomerAsset : ScriptableObject
{
    #region Custom Variables

    [System.Serializable]
    public class ExpressionInfo
    {
        public Sprite expressionIcon;
        public string expressionText;
        public float durationOfExpression;
    }

    [System.Serializable]
    public class CustomerInfo
    {
        public int          validFromLevel = 0;
        public Sprite       profilePicture;
        public GameObject   prefabReference;
    }

    #endregion

    #region Public Variables

    public ExpressionInfo[] happyExpression;

    [Space(5.0f)]
    public ExpressionInfo[] sadExpressions;

    [Space(5.0f)]
    public CustomerInfo[] customerInfos;

    #endregion

    #region Private Variables

    private int _numberOfCustomers = 0;

    #endregion

    #region OnScriptableObjectBehaviour

    private void OnEnable()
    {
        _numberOfCustomers = customerInfos.Length;
    }

    #endregion

    #region Configuretion

    private bool IsValidCustomerIndex(int customerIndex)
    {
        if (customerIndex >= 0 && customerIndex < customerInfos.Length)
            return true;

        return false;
    }

    private bool IsValidRequestBasedOnLevel(int customerIndex, int level) {

        if (IsValidCustomerIndex(customerIndex)) {

            if (level >= customerInfos[customerIndex].validFromLevel)
                return true;
        }

        return false;
    }

    

    private int GetRandomIndexForValidCustomer(int level = 0)
    {
        List<int> listOfValidCustomerIndex = new List<int>();
        for (int i = 0; i < _numberOfCustomers; i++)
        {
            if (IsValidRequestBasedOnLevel(i, level))
                listOfValidCustomerIndex.Add(i);
        }
        
        return listOfValidCustomerIndex[Random.Range(0, listOfValidCustomerIndex.Count)];
    }

    #endregion

    #region Public Callback

    public ExpressionInfo GetARandomHappyExpression
    {
        get { return happyExpression[Random.Range(0, happyExpression.Length)]; }
    }

    public ExpressionInfo GetARandomSadExpression
    {
        get { return sadExpressions[Random.Range(0, sadExpressions.Length)]; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="customerIndex"></param>
    /// <returns></returns>
    public GameObject Instanciate(ref int sharedCustomerIndex, ref Transform parent, Vector3 spawnWorldPosition = new Vector3(), int level = 0, int customerIndex = -1) {

        sharedCustomerIndex = IsValidRequestBasedOnLevel(customerIndex, level)?  customerIndex : GetRandomIndexForValidCustomer();

        GameObject newCustomer = Instantiate(customerInfos[sharedCustomerIndex].prefabReference, parent);
        newCustomer.transform.position = spawnWorldPosition;

        return newCustomer;

    }

    

    #endregion
}
