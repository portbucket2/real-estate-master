﻿using UnityEditor;
using com.faith.core;

[CustomEditor(typeof(NavMeshAgentControllerAsset))]
public class NavMeshAgentControllerAssetEditor : BaseEditorClass
{
    #region Private Variables

    private NavMeshAgentControllerAsset _reference;

    #endregion

    #region Editor

    public override void OnEnable()
    {
        _reference = (NavMeshAgentControllerAsset)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.LabelField("ActiveList : NavMeshAgent");
        EditorGUILayout.BeginVertical();
        {
            EditorGUI.indentLevel += 1;

            int numberOfRegisteredNavMesh = _reference.ListOfActiveNavMeshAgent.Count;
            for (int i = 0; i < numberOfRegisteredNavMesh; i++) {

                EditorGUILayout.LabelField("Entry (" + i + ")");
                EditorGUI.indentLevel += 1;
                EditorGUILayout.LabelField(_reference.ListOfActiveNavMeshAgent[i].ReferenceOfNavMeshAgent == null ? "NULL"  : _reference.ListOfActiveNavMeshAgent[i].ReferenceOfNavMeshAgent.name);
                EditorGUI.indentLevel -= 1;
            }

            EditorGUI.indentLevel -= 1;
        }
        EditorGUILayout.EndVertical();
        

        serializedObject.ApplyModifiedProperties();
    }

    #endregion
}
