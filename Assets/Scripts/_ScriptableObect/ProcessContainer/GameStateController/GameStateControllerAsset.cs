﻿using UnityEngine;
using System;
using System.Collections.Generic;
using com.faith.core;
[CreateAssetMenu(fileName = "GameStateControllerAsset", menuName = "RealEstateMaster/GameStateControllerAsset", order = 1)]
[DefaultExecutionOrder(Constant.EXECUTION_ORDER_STATEMACHINE)]
public class GameStateControllerAsset : ScriptableObject
{
    #region Public Variables

    public GameState gameState
    { get; private set; }

    public event Action     OnSceneLoadInitialization;
    public event Action     OnDataLoaded;
    public event Action     OnDayStarted;
    public event Action     OnCustomerArriving;
    public event Action     OnCustomerArrived;
    public event Action     OnAnalyzingComplete;
    public event Action     OnLandNotPurchased;
    public event Action     OnLandPurchased;
    public event Action     OnHousePlacementStarted;
    public event Action     OnHousePlacementEnded;
    public event Action     OnEnterInInventory;
    public event Action     OnExitFromInventory;
    public event Action     OnDecorationStarted;
    public event Action     OnDecorationEnded;
    public event Action     OnLandNotSold;
    public event Action     OnLandSold;
    public event Action     OnDayEnded;

    public event Action<GameState>      OnStateChanged;

    public GameConfiguratorAsset gameConfigeForState;


    #endregion

    #region Private Variables

    [SerializeField] private int _stackSize                = 32;
    [SerializeField] private List<GameState> _stackForGameState  = new List<GameState>();

    #endregion

    #region ScriptableObject

    public void OnEnable()
    {
        _stackForGameState = new List<GameState>();
    }

    #endregion

    #region Configuretion

    private bool IsValidStackIndex(int stackIndex) {
        return (stackIndex >= 0 && stackIndex < _stackSize)? true : false;
    }

    private void ManageStackForGameState(GameState gameState) {

        int stackCount = _stackForGameState.Count;
        if (stackCount > 0)
        {
            if (_stackForGameState[stackCount - 1] != gameState)
                _stackForGameState.Add(gameState);
        }
        else
        {
            _stackForGameState.Add(gameState);
        }

        if (stackCount >= _stackSize)
        {
            CoreDebugger.Debug.LogWarning("StackOverFlow : GameState. Removing(0) : "+ _stackForGameState[0] + ", gameState from the stack", gameConfigeForState);
            _stackForGameState.RemoveAt(0);
        }
    }

    #endregion

    #region Public Callback

    public void ChangeGameState(GameState gameState) {

        ManageStackForGameState(gameState);

        this.gameState = gameState;

        CoreDebugger.Debug.Log("GameStateChanged : " + gameState, gameConfigeForState);
        OnStateChanged?.Invoke(gameState);

        switch (gameState) {

            case GameState.SceneLoadInitialization:
                OnSceneLoadInitialization?.Invoke();
                break;

            case GameState.DataLoaded:
                OnDataLoaded?.Invoke();
                break;

            case GameState.DayStarted:
                OnDayStarted?.Invoke();
                break;

            case GameState.CustomerArriving:
                OnCustomerArriving?.Invoke();
                break;

            case GameState.CustomerArrived:
                OnCustomerArrived?.Invoke();
                break;

            case GameState.AnalyzingComplete:
                OnAnalyzingComplete?.Invoke();
                break;

            case GameState.LandNotPurchased:
                OnLandNotPurchased?.Invoke();
                break;

            case GameState.LandPurchased:
                OnLandPurchased?.Invoke();
                break;

            case GameState.HousePlacementStarted:
                OnHousePlacementStarted?.Invoke();
                break;

            case GameState.HousePlacementEnded:
                OnHousePlacementEnded?.Invoke();
                break;

            case GameState.EnterInInventory:
                OnEnterInInventory?.Invoke();
                break;

            case GameState.ExitFromInventory:
                OnExitFromInventory?.Invoke();
                break;

            case GameState.DecorationStarted:
                OnDecorationStarted?.Invoke();
                break;

            case GameState.DecorationEnded:
                OnDecorationEnded?.Invoke();
                break;

            case GameState.LandNotSold:
                OnLandNotSold?.Invoke();
                break;

            case GameState.LandSold:
                OnLandSold?.Invoke();
                break;

            case GameState.DayEnd:
                OnDayEnded?.Invoke();
                break;

        }
    }

    public GameState GetPreviousStateFromStack() {

        return GetStateFromStack(_stackForGameState.Count - 1);
    }

    public GameState GetStateFromStack(int stackIndex = -1) {

        if (IsValidStackIndex(stackIndex))
            return _stackForGameState[stackIndex];
        else {

            CoreDebugger.Debug.LogError("Invalid stackIndex = " + stackIndex + ", must be withing [0," + _stackSize + "]. returning 'None' State", gameConfigeForState);
            return GameState.None;
        }
    }

    #endregion
}
