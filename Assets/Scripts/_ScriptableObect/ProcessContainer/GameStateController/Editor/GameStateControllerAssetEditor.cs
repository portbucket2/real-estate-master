﻿
using UnityEngine;
using UnityEditor;
using com.faith.core;

[CustomEditor(typeof(GameStateControllerAsset))]
public class GameStateControllerAssetEditor : BaseEditorClass
{
    #region Private Variables

    private GameStateControllerAsset _reference;

    private SerializedProperty _gameConfigeForState;

    private SerializedProperty _stackSize;
    private SerializedProperty _stackForGameState;

    #endregion

    #region Editor

    public override void OnEnable()
    {
        base.OnEnable();

        _reference = (GameStateControllerAsset)target;

        if (_reference == null)
            return;

        _gameConfigeForState = serializedObject.FindProperty("gameConfigeForState");

        _stackSize = serializedObject.FindProperty("_stackSize");
        _stackForGameState = serializedObject.FindProperty("_stackForGameState");
    }

    public override void OnInspectorGUI()
    {

        ShowScriptReference();

        serializedObject.Update();

        EditorGUILayout.PropertyField(_gameConfigeForState);

        EditorGUILayout.Space();
        DrawHorizontalLine();

        EditorGUILayout.PropertyField(_stackSize);

        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.PropertyField(_stackForGameState);
        EditorGUI.EndDisabledGroup();

        serializedObject.ApplyModifiedProperties();
    }

    #endregion
}
