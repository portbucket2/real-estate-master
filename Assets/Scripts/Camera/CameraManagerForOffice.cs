﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faith.core;
using com.faith.camera;

public class CameraManagerForOffice : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnEnterInInventory()
    {
        base.OnEnterInInventory();

        //gameManager.cameraController.FocusCamera(
        //    new List<Transform>() { focusPositionForShelfView },
        //    originPositionForShelfView,
        //    t_CameraSettings: cameraSettingForShelfView.GetCameraSettings(0));

        SnapCamera(ref originPositionForShelfView, ref focusPositionForShelfView, ref cameraSettingForShelfView);
    }

    public override void OnExitFromInventory()
    {
        base.OnExitFromInventory();

        //gameManager.cameraController.FocusCamera(
        //    new List<Transform>() { focusPositionForOfficeView },
        //    originPositionForOfficeView,
        //    t_CameraSettings: cameraSettingForOfficeView.GetCameraSettings(0));

        SnapCamera(ref originPositionForOfficeView, ref focusPositionForOfficeView, ref cameraSettingForOfficeView);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    [Header("Parameter  :   OfficeView")]
    public Transform originPositionForOfficeView;
    public Transform focusPositionForOfficeView;
    public FaithCameraSettings cameraSettingForOfficeView;

    [Header("Parameter  :   ShelfView")]
    public Transform originPositionForShelfView;
    public Transform focusPositionForShelfView;
    public FaithCameraSettings cameraSettingForShelfView;

    #endregion

    #region Configuretion

    private void SnapCamera(ref Transform cameraOrigin, ref Transform cameraLookAtTarget, ref FaithCameraSettings cameraSettings) {

        gameManager.cameraController.cameraContainerTransformReference.position = cameraOrigin.position;
        gameManager.cameraController.cameraReference.transform.rotation = Quaternion.LookRotation(cameraLookAtTarget.position - cameraOrigin.position);
        gameManager.cameraController.cameraReference.fieldOfView = cameraSettings.GetCameraSettings(0).cameraFOV;
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
