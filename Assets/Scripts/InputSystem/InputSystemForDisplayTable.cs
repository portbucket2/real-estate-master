﻿using UnityEngine;
using com.faith.core;
using com.faith.gameplay.service;

public class InputSystemForDisplayTable : BaseInputSystem
{
    #region ALL UNITY FUNCTIONS

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnEnterInInventory()
    {
        base.OnEnterInInventory();

        CoreDebugger.Debug.Log("InputSystemEnabled : DisplayTable", gameManager.configAssetForInputSystem);

        _offsetToBeTravel               = landDisplayManager.offsetBetweenDisplayTable * offsetToBeTravel;
        _pixelSizeForOffsetToBeTravel   = Screen.width * pixelSizeForOffsetToBeTravel;
        _minBoundaryToBeOffset          = -((gameManager.inventory.NumberOfAvailableInventoryStack - 1) * landDisplayManager.offsetBetweenDisplayTable);

        GlobalTouchController.Instance.OnTouchDown += OnTouchDown;
        GlobalTouchController.Instance.OnTouch += OnTouch;
    }

    public override void OnExitFromInventory()
    {
        base.OnExitFromInventory();

        CoreDebugger.Debug.Log("InputSystemDisabled : DisplayTable", gameManager.configAssetForInputSystem);

        GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;
        GlobalTouchController.Instance.OnTouch -= OnTouch;
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public Camera mainCamera;
    public LayerMask layerForDisplayItem;

    [Space(5.0f)]
    public UIPanelForShelf      UIPanelForShelfReference;
    public DisplayTableManager   landDisplayManager;
    
    [Space(5.0f)]
    [Range(0.1f,1f)]
    public float offsetToBeTravel = 0.1f;
    [Range(0f,1f)]
    public float pixelSizeForOffsetToBeTravel = 0.1f;


    #endregion

    #region Private Variables

    private Vector3 _minBoundaryToBeOffset;
    private Vector3 _previousTouchPosition;
    private Vector3 _offsetToBeTravel;
    private float   _pixelSizeForOffsetToBeTravel;


    #endregion

    #region Configuretion

    private void OnLandQuickSell(int displayTableIndex, int displayItemIndex) {

        if (gameManager.inventory.RemoveItemFromInventory(displayTableIndex, displayItemIndex)) {

            landDisplayManager.PopulatingDisplayTable();
        }
    }

    private void OnLandSellToBuyer(int displayTableIndex, int displayItemIndex) {

        //We Need To Know The Price Before Removing From The Inventory
        float buyingPriceFromBuyer = gameManager.GetTheBuyingPriceForBuyer();

        if (gameManager.inventory.RemoveItemFromInventory(displayTableIndex, displayItemIndex))
        {
            landDisplayManager.PopulatingDisplayTable();
            gameManager.ChangeGameState(GameState.ExitFromInventory);
            gameManager.ChangeGameState(GameState.LandSold);


            gameManager.accountDisplay.AddBalance(buyingPriceFromBuyer);
        }
    }

    private void OnTouchDown(Vector3 touchPosition, int touchIndex)
    {
        CoreDebugger.Debug.Log("RayCasting On DisplayTable", gameManager.configAssetForInputSystem);

        _previousTouchPosition = touchPosition;

        Ray newRay = mainCamera.ScreenPointToRay(touchPosition);
        RaycastHit rayCastHit;
        if (Physics.Raycast(newRay, out rayCastHit, 100, layerForDisplayItem)) {

            DisplayItem displayItem = rayCastHit.collider.GetComponent<DisplayItem>();
            displayItem.MarkAsSelected(ref landDisplayManager.particleForSelection);
            int displayTableIndex = displayItem.displayTableIndex;
            int displayItemIndex = displayItem.displayItemIndex;


            sharedData.displayTableIndexForDecoringItem = displayTableIndex;
            sharedData.displayItemIndexForDecoringItem = displayItemIndex;

            UIPanelForShelfReference.ShowDisplayItemData(
                displayTableIndex,
                displayItemIndex,
                (tableIndex,itemIndex) => {

                    OnLandQuickSell(tableIndex, itemIndex);
                },
                (tableIndex, itemIndex) => {

                    OnLandSellToBuyer(tableIndex, itemIndex);
                });
        }
    }

    private void OnTouch(Vector3 touchPosition, int touchIndex) {


        float distance = Mathf.Abs(_previousTouchPosition.x - touchPosition.x);
        float interpolatedValue = Mathf.Clamp01(distance / _pixelSizeForOffsetToBeTravel);
        Vector3 offsetToBeTravel = _offsetToBeTravel * interpolatedValue;
        offsetToBeTravel *= ((touchPosition.x >= _previousTouchPosition.x) ? 1 : -1);
        Vector3 currentPosition = landDisplayManager.parentOfDisplayTable.localPosition;
        Vector3 newPosition = currentPosition + offsetToBeTravel;

        //CoreDebugger.Debug.LogWarning(string.Format(
        //    "Sliding DisplayTable {0} : {1} : {2} : {3} : {4}",
        //    distance,
        //    interpolatedValue,
        //    offsetToBeTravel,
        //    currentPosition,
        //    newPosition), gameManager.configAssetForInputSystem);

        if (newPosition.x >= _minBoundaryToBeOffset.x && newPosition.x <= 0
        && newPosition.y >= _minBoundaryToBeOffset.y && newPosition.y <= 0
        && newPosition.z >= _minBoundaryToBeOffset.z && newPosition.z <= 0)
        {

            landDisplayManager.MoveDisplayTable(offsetToBeTravel);
        }
        else
        {
            Vector3 difference = new Vector3(
                    Mathf.Min(Mathf.Abs(0 - currentPosition.x), Mathf.Abs(_minBoundaryToBeOffset.x - currentPosition.x)),
                    Mathf.Min(Mathf.Abs(0 - currentPosition.y), Mathf.Abs(_minBoundaryToBeOffset.y - currentPosition.y)),
                    Mathf.Min(Mathf.Abs(0 - currentPosition.z), Mathf.Abs(_minBoundaryToBeOffset.z - currentPosition.z))
                );
            difference *= (touchPosition.x >= _previousTouchPosition.x) ? 1 : -1;
            landDisplayManager.MoveDisplayTable(difference);
        }

        _previousTouchPosition = touchPosition;

    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
