﻿using UnityEngine;
using com.faith.core;
using com.faith.gameplay.service;
public class InputSystemForBuyer : BaseInputSystem
{
    #region ALL UNITY FUNCTIONS


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnCustomerArrived()
    {
        base.OnCustomerArrived();

        if (sharedData.currentCustomerType == CustomerType.Buyer) {

            if (!gameManager.IsInputSystemInterrupted)
                EnablInputSystemForBuyer();
            else {
                WaitUntilInterruptionRevoked(() =>
                {
                    EnablInputSystemForBuyer();
                });
            }
        } 
    }

    public override void OnLandNotSold()
    {
        base.OnLandNotSold();

        DisableInputSystemForBuyer();
    }

    public override void OnLandSold()
    {
        base.OnLandSold();

        DisableInputSystemForBuyer();
    }

    public override void OnEnterInInventory()
    {
        base.OnEnterInInventory();

        DisableInputSystemForBuyer();
    }

    public override void OnExitFromInventory()
    {
        base.OnExitFromInventory();

        if (sharedData.currentCustomerType == CustomerType.Buyer) {

            EnablInputSystemForBuyer();
        }
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Private Variables

    private bool _isTouchDownDetectedAfterEnable = false;
    private Vector3 _touchDownPosition;

    #endregion

    #region Configuretion

    private void EnablInputSystemForBuyer()
    {
        CoreDebugger.Debug.Log("InputSystemEnabled : Buyer", gameManager.configAssetForInputSystem);

        GlobalTouchController.Instance.OnTouchDown += OnTouchDown;
        GlobalTouchController.Instance.OnTouchUp += OnTouchUp;
    }

    private void DisableInputSystemForBuyer()
    {
        CoreDebugger.Debug.Log("InputSystemDisabled : Buyer", gameManager.configAssetForInputSystem);

        GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;
        GlobalTouchController.Instance.OnTouchUp -= OnTouchUp;

        _isTouchDownDetectedAfterEnable = false;
    }

    private void OnTouchDown(Vector3 touchPosition, int touchIndex)
    {
        _touchDownPosition = touchPosition;
        _isTouchDownDetectedAfterEnable = true;
    }

    private void OnTouchUp(Vector3 touchPosition, int touchIndex)
    {
        if (!IsInputSystemInterrupted && _isTouchDownDetectedAfterEnable && Mathf.Abs(_touchDownPosition.x - touchPosition.x) >= pixelThreshold)
        {
            
            if (touchPosition.x >= _touchDownPosition.x)
            {
                sharedData.shelfMode = ShelfMode.SellingToBuyer;
                gameManager.ChangeGameState(GameState.EnterInInventory);
            }
            else
            {
                DisableInputSystemForBuyer();
                gameManager.ChangeGameState(GameState.LandNotSold);
            }
        }
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
