﻿using UnityEngine;
using com.faith.core;
using com.faith.gameplay.service;
public class InputSystemForSeller : BaseInputSystem
{
    #region ALL UNITY FUNCTIONS


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnAnalyzingComplete()
    {
        base.OnAnalyzingComplete();

        sharedData.isAnalyzingCompleteForSellerCustomer = true;

        if (sharedData.currentCustomerType == CustomerType.Seller)
        {
            if (!gameManager.IsInputSystemInterrupted)
                EnablInputSystemForSeller();
            else
            {
                WaitUntilInterruptionRevoked(() =>
                {
                    EnablInputSystemForSeller();
                });
            }
        }
    }
    public override void OnLandNotPurchased()
    {
        base.OnLandNotPurchased();

        DisableInputSystemForSeller();
    }

    public override void OnLandPurchased()
    {
        base.OnLandPurchased();

        DisableInputSystemForSeller();
    }

    public override void OnEnterInInventory()
    {
        base.OnEnterInInventory();

        DisableInputSystemForSeller();
    }

    public override void OnExitFromInventory()
    {
        base.OnExitFromInventory();

        if (sharedData.currentCustomerType == CustomerType.Seller && sharedData.isAnalyzingCompleteForSellerCustomer) {
            EnablInputSystemForSeller();
        }
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Private Variables

    private bool    _isTouchDownDetectedAfterEnable = false;
    private Vector3 touchDownPosition;

    #endregion

    #region Configuretion

    private void EnablInputSystemForSeller() {

        CoreDebugger.Debug.Log("InputSystemEnabled : Seller", gameManager.configAssetForInputSystem);

        GlobalTouchController.Instance.OnTouchDown += OnTouchDown;
        GlobalTouchController.Instance.OnTouchUp += OnTouchUp;
    }

    private void DisableInputSystemForSeller() {

        CoreDebugger.Debug.Log("InputSystemDisabled : Seller", gameManager.configAssetForInputSystem);

        GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;
        GlobalTouchController.Instance.OnTouchUp -= OnTouchUp;

        _isTouchDownDetectedAfterEnable = false;
    }


    private void OnTouchDown(Vector3 touchPosition, int touchIndex) {

        touchDownPosition = touchPosition;
        _isTouchDownDetectedAfterEnable = true;
    }

    private void OnTouchUp(Vector3 touchPosition, int touchIndex) {

        if (!IsInputSystemInterrupted && _isTouchDownDetectedAfterEnable && Mathf.Abs(touchDownPosition.x - touchPosition.x) >= pixelThreshold)
        {
            if (touchPosition.x >= touchDownPosition.x)
            {
                if (gameManager.inventory.IsEmptySlotAvailable() >= 0)
                {
                    if (gameManager.accountDisplay.DeductBalance(sharedData.landAssetOnSeller.sellingPrice))
                    {
                        DisableInputSystemForSeller();
                        gameManager.ChangeGameState(GameState.LandPurchased);
                    }
                }
                else {

                    gameManager.popUpPanel.ShowPopUp(
                            gameManager.popUpPanelInfoForNoEmptySlotInInventory,
                            gameManager.inventory.GetPurchasePriceForBuyingInventoryStack() + "$",
                            () =>
                            {
                                if (gameManager.accountDisplay.DeductBalance(gameManager.inventory.GetPurchasePriceForBuyingInventoryStack()))
                                {
                                    gameManager.inventory.PurchaseDisplayTableForInventory();
                                    CoreDebugger.Debug.LogWarning("Purchase 'DisplayTable', and showing it by changing state to 'EnterInventory'", gameManager.configAssetForFeatureUnderDevelopment);
                                }
                            });
                }
            }
            else
            {
                DisableInputSystemForSeller();
                gameManager.ChangeGameState(GameState.LandNotPurchased);
            }
        }
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
