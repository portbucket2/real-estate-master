﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using com.faith.core;
[DefaultExecutionOrder(Constant.EXECUTION_ORDER_RealEstateMasterBehaviour)]
public abstract class BaseRealEstateMasterBehaviour : MonoBehaviour
{

    #region Private Variables

    private bool hasRegistered = false;

    protected GameManager gameManager;

    protected SharedData sharedData {
        get
        { return gameManager.levelData.sharedGameData;
        }
        set {
            gameManager.levelData.sharedGameData = value;
        }
    }

    protected bool IsInputSystemInterrupted { get { return BaseInputSystem.IsInputSystemInterrupted; } }

    #endregion

    #region Public Callback



    public void Register()
    {
        if (!hasRegistered)
        {

            gameManager = GameManager.Instance;

            gameManager.gameStateController.OnSceneLoadInitialization += OnSceneLoadInitialization;
            gameManager.gameStateController.OnDataLoaded += OnDataLoaded;
            gameManager.gameStateController.OnDayStarted += OnDayStarted;
            gameManager.gameStateController.OnCustomerArriving += OnCustomerArriving;
            gameManager.gameStateController.OnCustomerArrived += OnCustomerArrived;
            gameManager.gameStateController.OnAnalyzingComplete += OnAnalyzingComplete;
            gameManager.gameStateController.OnLandNotPurchased += OnLandNotPurchased;
            gameManager.gameStateController.OnLandPurchased += OnLandPurchased;
            gameManager.gameStateController.OnHousePlacementStarted += OnHousePlacementStarted;
            gameManager.gameStateController.OnHousePlacementEnded += OnHousePlacementEnded;
            gameManager.gameStateController.OnEnterInInventory += OnEnterInInventory;
            gameManager.gameStateController.OnExitFromInventory += OnExitFromInventory;
            gameManager.gameStateController.OnDecorationStarted += OnDecorationStarted;
            gameManager.gameStateController.OnDecorationEnded += OnDecorationEnded;
            gameManager.gameStateController.OnLandNotSold += OnLandNotSold;
            gameManager.gameStateController.OnLandSold += OnLandSold;
            gameManager.gameStateController.OnDayEnded += OnDayEnded;
            gameManager.gameStateController.OnStateChanged += OnStateChanged;



            hasRegistered = true;
        }
    }

    public void Unregister()
    {

        if (hasRegistered)
        {
            gameManager.gameStateController.OnSceneLoadInitialization -= OnSceneLoadInitialization;
            gameManager.gameStateController.OnDataLoaded -= OnDataLoaded;
            gameManager.gameStateController.OnDayStarted -= OnDayStarted;
            gameManager.gameStateController.OnCustomerArriving -= OnCustomerArriving;
            gameManager.gameStateController.OnCustomerArrived -= OnCustomerArrived;
            gameManager.gameStateController.OnAnalyzingComplete -= OnAnalyzingComplete;
            gameManager.gameStateController.OnLandNotPurchased -= OnLandNotPurchased;
            gameManager.gameStateController.OnLandPurchased -= OnLandPurchased;
            gameManager.gameStateController.OnHousePlacementStarted -= OnHousePlacementStarted;
            gameManager.gameStateController.OnHousePlacementEnded -= OnHousePlacementEnded;
            gameManager.gameStateController.OnEnterInInventory -= OnEnterInInventory;
            gameManager.gameStateController.OnExitFromInventory -= OnExitFromInventory;
            gameManager.gameStateController.OnDecorationStarted -= OnDecorationStarted;
            gameManager.gameStateController.OnDecorationEnded -= OnDecorationEnded;
            gameManager.gameStateController.OnLandNotSold -= OnLandNotSold;
            gameManager.gameStateController.OnLandSold -= OnLandSold;
            gameManager.gameStateController.OnDayEnded -= OnDayEnded;
            gameManager.gameStateController.OnStateChanged -= OnStateChanged;


            hasRegistered = false;
        }
    }

    #endregion

    #region Configuretion


    private IEnumerator ControllerForWaitingUntilInterruptionRevoked(UnityAction OnInterruptionRevoked)
    {
        WaitUntil _waitUntil = new WaitUntil(() =>
        {
            if (BaseInputSystem.IsInputSystemInterrupted)
                return false;

            return true;
        });

        yield return _waitUntil;
        OnInterruptionRevoked?.Invoke();
        StopCoroutine(ControllerForWaitingUntilInterruptionRevoked(null));
    }

    #endregion

    #region MonoBehaviour

    public virtual void Awake() {

        
    }

    public virtual void OnEnable()
    {
        Register();
    }

    public virtual void OnDisable()
    {
        Unregister();
    }

    public virtual void OnDestroy() {

        Unregister();
    }

    #endregion

    #region VirtualFunction

    public virtual void OnSceneLoadInitialization() {


    }

    public virtual void OnDataLoaded()
    {


    }

    public virtual void OnDayStarted()
    {


    }

    public virtual void OnCustomerArriving() {

    }

    public virtual void OnCustomerArrived() {


    }

    public virtual void OnAnalyzingComplete() {

    }

    public virtual void OnLandNotPurchased()
    {

    }

    public virtual void OnLandPurchased() {

    }

    public virtual void OnHousePlacementStarted() {

    }

    public virtual void OnHousePlacementEnded() {

    }

    public virtual void OnEnterInInventory() {

    }

    public virtual void OnExitFromInventory() {


    }

    public virtual void OnDecorationStarted() {


    }

    public virtual void OnDecorationEnded() {


    }

    public virtual void OnLandNotSold()
    {

    }

    public virtual void OnLandSold() {

    }

    public virtual void OnDayEnded()
    {


    }

    public virtual void OnStateChanged(GameState gameState) {


    }

    #endregion

    #region Protected callback

    protected void WaitUntilInterruptionRevoked(UnityAction OnInterruptionRevoked)
    {
        StartCoroutine(ControllerForWaitingUntilInterruptionRevoked(OnInterruptionRevoked));
    }

    protected void SetStatusFlagForInputSystem(bool isInterrupted)
    {
        CoreDebugger.Debug.Log("InterruptionStatus : " + isInterrupted, gameManager.configAssetForInputSystem);
        BaseInputSystem.IsInputSystemInterrupted = isInterrupted;
    }

    protected void UIAppear(Transform _transformReference, UnityAction OnTransitionEnd = null)
    {

        gameManager.centralAnimatorForUIAppear.DoAnimate(
                new List<Transform>() { _transformReference },
                OnTransitionEnd
            );
    }

    protected void UIDisappear(Transform _transformReference, UnityAction OnTransitionEnd = null)
    {

        gameManager.centralAnimatorForUIDisappear.DoAnimate(
                new List<Transform>() { _transformReference },
                OnTransitionEnd
            );
    }

    protected void UIInteract(Transform _transformReference, UnityAction OnTransitionEnd = null) {

        gameManager.centralAnimatorForUIInteract.DoAnimate(
                new List<Transform>() { _transformReference },
                OnTransitionEnd
            );
    }

    #endregion

}
