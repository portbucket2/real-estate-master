﻿[System.Serializable]
public class SharedData
{
    public int dayIndex = 0;
    public int levelIndex = 0;
    public bool flagForVisitedTheLandForDecoration = false;

    public ShelfMode shelfMode = ShelfMode.Browsing;

    public bool isAnalyzingCompleteForSellerCustomer = false;
    public int  customerIndex = 0;
    public CustomerType currentCustomerType = CustomerType.Seller;

    

    //Counter   :   LandSellerAgent
    public int numberOfLandSellerAgent = -1;
    public int remainingNumberOfLandSellerAgent = -1;

    //Counter   :   LandBuyerAgent
    public int numberOfLandBuyerAgent = -1;
    public int remainingNumberOfLandBuyerAgent = -1;

    public int decorationTypeIndexForRecentPurchasedLand = -1;
    public int decorationItemIndexForRecentPurchasedLand = -1;

    //It will either be assigned by User or auto assigned after the house is decorated
    public int displayTableIndexForDecoringItem = -1;
    public int displayItemIndexForDecoringItem  = -1;

    //Counter   :   CashEarned and Expense
    public double totalMoneyEarned = 0;
    public double totalMoneySpent = 0;


    public EnumAssetForLand landAssetOnSeller;
    public BaseEnumAsset    landTypeFromBuyer;
    

    
}

public class Constant
{
    
    public const int EXECUTION_ORDER_GAMEMANAGER = -1000;
    public const int EXECUTION_ORDER_STATEMACHINE = -900;
    public const int EXECUTION_ORDER_RealEstateMasterBehaviour = -100;
}
