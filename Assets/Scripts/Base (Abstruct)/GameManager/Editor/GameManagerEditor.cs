﻿using UnityEngine;
using UnityEditor;
using com.faith.core;

[CustomEditor(typeof(GameManager))]
public class GameManagerEdigtor : BaseEditorClass
{

    #region Private Variables

    private GameManager _reference;

    private SerializedProperty _sp_GameState;
    private SerializedProperty _sp_ListOfDisabledRealEstateMasterBehaviour;

    private SerializedProperty _sp_cameraController;
    private SerializedProperty _sp_initialDelayForSceneLoaded;

    private SerializedProperty _sp_accountDisplay;
    private SerializedProperty _sp_popUpPanel;

    private SerializedProperty _sp_configAssetForInputSystem;
    private SerializedProperty _sp_configAssetForFeatureUnderDevelopment;

    private SerializedProperty _sp_popUpPanelInfoForNoEmptySlotInInventory;

    private SerializedProperty _sp_GameStateController;
    private SerializedProperty _sp_LevelData;
    private SerializedProperty _sp_LandAssets;
    private SerializedProperty _sp_inventory;

    private SerializedProperty _sp_centralAnimatorForAppear;
    private SerializedProperty _sp_centralAnimatorForDisappear;

    private SerializedProperty _sp_centralAnimatorForUIAppear;
    private SerializedProperty _sp_centralAnimatorForUIDisappear;
    private SerializedProperty _sp_centralAnimatorForUIInteract;

    #endregion

    #region Editor

    public override void OnEnable()
    {
        _reference = (GameManager)target;

        if (_reference == null)
            return;

        _sp_GameState = serializedObject.FindProperty("gameState");
        _sp_ListOfDisabledRealEstateMasterBehaviour = serializedObject.FindProperty("_listOfRealEstateMasterBehaviour");

        _sp_cameraController = serializedObject.FindProperty("cameraController");
        _sp_initialDelayForSceneLoaded = serializedObject.FindProperty("initialDelayForSceneLoaded");

        _sp_accountDisplay = serializedObject.FindProperty("accountDisplay");
        _sp_popUpPanel = serializedObject.FindProperty("popUpPanel");

        _sp_configAssetForInputSystem = serializedObject.FindProperty("configAssetForInputSystem");
        _sp_configAssetForFeatureUnderDevelopment = serializedObject.FindProperty("configAssetForFeatureUnderDevelopment");

        _sp_popUpPanelInfoForNoEmptySlotInInventory = serializedObject.FindProperty("popUpPanelInfoForNoEmptySlotInInventory");

        _sp_GameStateController = serializedObject.FindProperty("gameStateController");
        _sp_LevelData = serializedObject.FindProperty("levelData");
        _sp_LandAssets = serializedObject.FindProperty("landAssets");
        _sp_inventory = serializedObject.FindProperty("inventory");

        _sp_centralAnimatorForUIAppear = serializedObject.FindProperty("centralAnimatorForUIAppear");
        _sp_centralAnimatorForUIDisappear = serializedObject.FindProperty("centralAnimatorForUIDisappear");
        _sp_centralAnimatorForUIInteract = serializedObject.FindProperty("centralAnimatorForUIInteract");

        _sp_centralAnimatorForAppear = serializedObject.FindProperty("centralAnimatorForAppear");
        _sp_centralAnimatorForDisappear = serializedObject.FindProperty("centralAnimatorForDisappear");

    }

    public override void OnInspectorGUI()
    {
        ShowScriptReference();

        serializedObject.Update();

        _reference.showDeveloperPanel = EditorGUILayout.Foldout(
                    _reference.showDeveloperPanel,
                    "DeveloperPanel",
                    true
                );

        if (_reference.showDeveloperPanel)
        {

            EditorGUI.indentLevel += 1;

            if (EditorApplication.isPlaying)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUI.BeginChangeCheck();
                    EditorGUILayout.PropertyField(_sp_GameState);
                    if (EditorGUI.EndChangeCheck())
                    {

                        _sp_GameState.serializedObject.ApplyModifiedProperties();
                        EditorUtility.SetDirty(target);

                        _reference.ChangeGameState(_reference.gameState);
                    }
                }
                EditorGUILayout.EndHorizontal();

            }
            else {

                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.HelpBox(
                        "By pressing the 'Find' button, you will be able to baked the 'Disabled' gameObject (Child of 'GameManager') on your Scene/Prefab",
                        MessageType.Info);
                    if (GUILayout.Button("Find", GUILayout.Width(100)))
                    {
                        BaseRealEstateMasterBehaviour[] baseRealEstateMasterBehaviours = _reference.transform.GetComponentsInChildren<BaseRealEstateMasterBehaviour>(true);
                        int arraySize = baseRealEstateMasterBehaviours.Length;
                        _sp_ListOfDisabledRealEstateMasterBehaviour.arraySize = arraySize;
                        for (int i = 0; i < arraySize; i++) {

                            _sp_ListOfDisabledRealEstateMasterBehaviour.GetArrayElementAtIndex(i).objectReferenceValue = baseRealEstateMasterBehaviours[i];
                        }

                        _sp_ListOfDisabledRealEstateMasterBehaviour.serializedObject.ApplyModifiedProperties();
                    }
                }
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();
            DrawHorizontalLine();
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(_sp_ListOfDisabledRealEstateMasterBehaviour);
            EditorGUI.EndDisabledGroup();

            EditorGUI.indentLevel -= 1;
        }

        EditorGUILayout.Space();
        DrawHorizontalLine();
        EditorGUILayout.LabelField("GlobalReference :   Others", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_sp_cameraController);
        EditorGUILayout.PropertyField(_sp_initialDelayForSceneLoaded);

        EditorGUILayout.Space();
        DrawHorizontalLine();
        EditorGUILayout.LabelField("GlobalReference :   UI", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_sp_accountDisplay);
        EditorGUILayout.PropertyField(_sp_popUpPanel);

        EditorGUILayout.Space();
        DrawHorizontalLine();
        EditorGUILayout.LabelField("GameConfig      :   Components", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_sp_configAssetForFeatureUnderDevelopment);
        EditorGUILayout.PropertyField(_sp_configAssetForInputSystem);

        EditorGUILayout.Space();
        DrawHorizontalLine();
        EditorGUILayout.LabelField("PopUpPanelInfo  :   UI", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_sp_popUpPanelInfoForNoEmptySlotInInventory);

        EditorGUILayout.Space();
        DrawHorizontalLine();
        EditorGUILayout.LabelField("GameAsset       :   Core", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_sp_GameStateController);
        EditorGUILayout.PropertyField(_sp_LevelData);
        EditorGUILayout.PropertyField(_sp_LandAssets);
        EditorGUILayout.PropertyField(_sp_inventory);

        EditorGUILayout.Space();
        DrawHorizontalLine();
        EditorGUILayout.LabelField("AnimatorAsset   :   UI", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_sp_centralAnimatorForUIAppear);
        EditorGUILayout.PropertyField(_sp_centralAnimatorForUIDisappear);
        EditorGUILayout.PropertyField(_sp_centralAnimatorForUIInteract);

        EditorGUILayout.Space();
        DrawHorizontalLine();
        EditorGUILayout.LabelField("AnimatorAsset   :   World", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_sp_centralAnimatorForAppear);
        EditorGUILayout.PropertyField(_sp_centralAnimatorForDisappear);

        serializedObject.ApplyModifiedProperties();
    }

    #endregion

}
