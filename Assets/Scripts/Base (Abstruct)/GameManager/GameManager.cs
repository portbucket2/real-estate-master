﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faith.core;
using com.faith.camera;

[RequireComponent(typeof(GameConfiguratorManager))]
[DefaultExecutionOrder(Constant.EXECUTION_ORDER_GAMEMANAGER)]
public class GameManager : BaseRealEstateMasterBehaviour
{

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        Initialization();
    }


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnDayStarted()
    {
        int currentDay = levelData.sharedGameData.dayIndex;

        levelData.sharedGameData.numberOfLandSellerAgent            = levelData.listOfLevelData[currentDay].numberOfSellerLandAgent;
        levelData.sharedGameData.remainingNumberOfLandSellerAgent   = levelData.sharedGameData.numberOfLandSellerAgent;

        levelData.sharedGameData.numberOfLandBuyerAgent = levelData.listOfLevelData[currentDay].numberOfBuyerLandAgent;
        levelData.sharedGameData.remainingNumberOfLandBuyerAgent = levelData.sharedGameData.numberOfLandBuyerAgent;

    }


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variable

    public static GameManager Instance;

#if UNITY_EDITOR

    public bool showDeveloperPanel;
    public GameState gameState;

#endif

    public new bool IsInputSystemInterrupted
    {
        get { return BaseInputSystem.IsInputSystemInterrupted; }
    }

    public GameState CurrentGameState
    {
        get { return gameStateController.gameState; }
    }

    public GameState PreviousGameState
    {
        get { return gameStateController.GetPreviousStateFromStack(); }
    }

    public SharedData SharedGameData
    {
        get { return levelData.sharedGameData; }
    }

    public FaithCameraController cameraController;
    public FloatReference initialDelayForSceneLoaded;

    public UIAccountDisplay accountDisplay;
    public UIPopPanel popUpPanel;

    public GameConfiguratorAsset configAssetForFeatureUnderDevelopment;
    public GameConfiguratorAsset configAssetForInputSystem;

    public PopUpPanelInfo popUpPanelInfoForNoEmptySlotInInventory;

    public GameStateControllerAsset gameStateController;
    public LevelDataAsset levelData;
    public LandAssets landAssets;
    public Inventory inventory;

    public TransformAnimatorAsset centralAnimatorForUIAppear;
    public TransformAnimatorAsset centralAnimatorForUIDisappear;
    public TransformAnimatorAsset centralAnimatorForUIInteract;

    public TransformAnimatorAsset centralAnimatorForAppear;
    public TransformAnimatorAsset centralAnimatorForDisappear;

    #endregion

    #region Private Variables

    private static bool _isInitialDataLoaded = false;

    [SerializeField] private List<BaseRealEstateMasterBehaviour> _listOfRealEstateMasterBehaviour;

    #endregion

    #region Configuretion

    private void Initialization()
    {
        Instance = this;

        foreach (BaseRealEstateMasterBehaviour realEstateMasterBehaviour in _listOfRealEstateMasterBehaviour)
        {
            realEstateMasterBehaviour.Register();
        }

        if (!_isInitialDataLoaded) {

            levelData.Initialization(delegate {

                inventory.Initialization();
                gameStateController.ChangeGameState(GameState.DataLoaded);
                gameStateController.ChangeGameState(GameState.SceneLoadInitialization);
                _isInitialDataLoaded = true;
            });
        }
    }

    #endregion

    #region PublicCallback

    public void ChangeGameState(GameState gameState)
    {
        gameStateController.ChangeGameState(gameState);
    }

    public float GetEstimatedPriceForLand(int landUniqueID, int displayTableIndex, int displayItemIndex)
    {
        bool isDecorated = inventory.IsHouseDecorated(displayTableIndex, displayItemIndex);
        return landAssets.listOfLandAssetInfo[landUniqueID].estimatedPrice * (isDecorated ? Mathf.Clamp(landAssets.landValueIncreaseAfterDecoration, 1, Mathf.Infinity) : 1);
    }

    public bool IsAnyLandAvailableForBuyer() {

        int numberOfAvailableDisplayTable = inventory.NumberOfAvailableInventoryStack;
        for (int i = 0; i < numberOfAvailableDisplayTable; i++)
        {

            int numberOfItemInDisplayTable = inventory.NumberOfItemInDisplayTable(i);
            for (int j = 0; j < numberOfItemInDisplayTable; j++)
            {
                int landUniqueID = gameManager.inventory.GetLandUniqueID(i, j);
                if (sharedData.landTypeFromBuyer == landAssets.listOfLandAssetInfo[landUniqueID].landType)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public float GetTheBuyingPriceForBuyer() {

        bool hasFoundTheItemInDisplayTable = false;
        float result = Mathf.NegativeInfinity;
        int numberOfAvailableDisplayTable = inventory.NumberOfAvailableInventoryStack;
        for (int i = 0; i < numberOfAvailableDisplayTable; i++)
        {

            int numberOfItemInDisplayTable = inventory.NumberOfItemInDisplayTable(i);
            for (int j = 0; j < numberOfItemInDisplayTable; j++)
            {
                int landUniqueID = gameManager.inventory.GetLandUniqueID(i, j);
                if (sharedData.landTypeFromBuyer == landAssets.listOfLandAssetInfo[landUniqueID].landType) {

                    result = Mathf.Max(result, GetEstimatedPriceForLand(landUniqueID, i, j));
                    hasFoundTheItemInDisplayTable = true;
                }
            }
        }

        return hasFoundTheItemInDisplayTable ? result : (sharedData.dayIndex + 1) * 1000;
    }

    public void LoadScene(
        SceneReference sceneReference,
        UnityAction OnSceneLoaded = null,
        float initialDelayToInvokeOnSceneLoaded = -1) {

        initialDelayToInvokeOnSceneLoaded = initialDelayToInvokeOnSceneLoaded == -1 ? initialDelayForSceneLoaded : initialDelayToInvokeOnSceneLoaded;

        sceneReference.LoadScene(
            OnUpdatingProgression : null,
            OnSceneLoaded : () => {
                gameManager.ChangeGameState(GameState.SceneLoadInitialization);
                OnSceneLoaded?.Invoke();
            },
            initalDelayToInvokeOnSceneLoaded : initialDelayToInvokeOnSceneLoaded);

            
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS


}
