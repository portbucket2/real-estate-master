﻿public enum GameState
{
    None,
    SceneLoadInitialization,
    DataLoaded,
    DayStarted,
    CustomerArriving,
    CustomerArrived,
    AnalyzingComplete,
    LandNotPurchased,
    LandPurchased,
    HousePlacementStarted,
    HousePlacementEnded,
    EnterInInventory,
    ExitFromInventory,
    DecorationStarted,
    DecorationEnded,
    LandNotSold,
    LandSold,
    DayEnd
}

public enum CustomerType
{
    Seller,
    Buyer
}

public enum ShelfMode
{
    Browsing,
    SellingToBuyer
}