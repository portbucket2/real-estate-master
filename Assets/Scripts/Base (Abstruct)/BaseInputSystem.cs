﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using com.faith.gameplay.service;

public abstract class BaseInputSystem : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnDayStarted()
    {
        base.OnDayStarted();
        GlobalTouchController.Instance.EnableTouchController();
    }

    public override void OnCustomerArriving()
    {
        base.OnCustomerArriving();
        GlobalTouchController.Instance.EnableTouchController();
    }

    public override void OnDayEnded()
    {
        base.OnDayEnded();
        GlobalTouchController.Instance.DisableTouchController();
    }

    public override void OnStateChanged(GameState gameState)
    {
        base.OnStateChanged(gameState);

        switch (gameState)
        {
            case GameState.SceneLoadInitialization:
                SetStatusFlagForInputSystem(false);
                break;
            case GameState.EnterInInventory:
                SetStatusFlagForInputSystem(true);
                break;
            case GameState.ExitFromInventory:

                SetStatusFlagForInputSystem(false);
                break;
        }
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables


    [Header("InputSystem    :   Accept/Decline SellerDeal")]
    [Range(0,256)]
    public float pixelThreshold = 100;

    #endregion

    #region Private/Proctected Variables

    public static bool IsInputSystemInterrupted;


    #endregion


    #region Configuretion


    private IEnumerator ControllerForWaitingUntilInterruptionRevoked(UnityAction OnInterruptionRevoked)
    {
        WaitUntil _waitUntil = new WaitUntil(() =>
        {
            if (IsInputSystemInterrupted)
                return false;

            return true;
        });

        yield return _waitUntil;
        OnInterruptionRevoked?.Invoke();
        StopCoroutine(ControllerForWaitingUntilInterruptionRevoked(null));
    }

    #endregion

    #region Protected Callback

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
