﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using com.faith.core;

public class UIBaseManager : BaseRealEstateMasterBehaviour
{

    #region ALL UNITY FUNCTIONS

    public override void Awake()
    {
        base.Awake();

        SceneManager.sceneLoaded    += OnSceneLoaded;
        SceneManager.sceneUnloaded  += OnSceneUnloaded;
    }

    public virtual void Start() {


    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    [Header("OnSceneLoaded")]
    [Range(0.1f,2f)]
    public float durationForShowingLoadSceneAnimation;

    [Space(5.0f)]
    public RectTransform[] rectTransforms;
    public Image[] images;

    #endregion

    #region Configuretion

    private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        StartCoroutine(ControllerForShowingLoadSceneAnimation());
    }

    private void OnSceneUnloaded(Scene scene)
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        SceneManager.sceneUnloaded -= OnSceneUnloaded;
    }

    private IEnumerator ControllerForShowingLoadSceneAnimation() {

        float remainingTime         = durationForShowingLoadSceneAnimation;
        float progression           = 0;
        float cycleLength           = 0.0167f;
        WaitForSeconds cycleDelay   = new WaitForSeconds(cycleLength);

        while (remainingTime > 0) {

            progression = 1f - (remainingTime / durationForShowingLoadSceneAnimation);

            AppearAnimationByValue(progression);

            yield return cycleDelay;
            remainingTime -= cycleLength;
        }

        AppearAnimationByValue(1);

        StopCoroutine(ControllerForShowingLoadSceneAnimation());
    }

    #endregion

    #region Public Callback

    public void AppearAnimationByValue(float value) {

        foreach (RectTransform rectTransform in rectTransforms)
        {
            rectTransform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, value);
        }

        foreach (Image image in images)
        {

            Color alphaZero = image.color;
            alphaZero.a = 0;

            Color alphaOne = image.color;
            alphaOne.a = 1;
            image.color = Color.Lerp(alphaZero, alphaOne, 1f - value);

            if (image.type == Image.Type.Filled)
                image.fillAmount = 1f - value;
        }
    }

    public void DisappearAnimationByValue(float value) {

        AppearAnimationByValue(1f - value);
    }


    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS
}
