﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;
using com.faith.core;

[RequireComponent(typeof(NavMeshAgent))]
public class CustomerController : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        sharedNavMeshAttribute.AssignValue(ref _navMeshAgent);
    }


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnCustomerArriving()
    {
        base.OnCustomerArriving();

        sharedData.isAnalyzingCompleteForSellerCustomer = false;
    }

    public override void OnCustomerArrived()
    {
        if (!_isCustomerAlreadyVisitedTheOffice) {

            animator.SetTrigger(ANIMATION_KEY_IDLE);
            _isCustomerAlreadyVisitedTheOffice = true;
        }
        
    }

    public override void OnLandNotPurchased()
    {
        base.OnLandNotPurchased();

        animator.SetTrigger(ANIMATION_KEY_REJECT1);
        
    }

    public override void OnLandPurchased()
    {
        base.OnLandPurchased();

        animator.SetTrigger(ANIMATION_KEY_WIN1);
    }

    public override void OnLandNotSold()
    {
        base.OnLandNotSold();

        animator.SetTrigger(ANIMATION_KEY_REJECT1);
    }

    public override void OnLandSold()
    {
        base.OnLandSold();

        animator.SetTrigger(ANIMATION_KEY_WIN1);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public Animator                         animator;
    public NavMeshAgentSharedAttributeAsset sharedNavMeshAttribute;

    #endregion

    #region Private Variables

    private const string ANIMATION_KEY_IDLE = "Idle";
    private const string ANIMATION_KEY_WIN1 = "Win1";
    private const string ANIMATION_KEY_REJECT1 = "Reject1";
    private const string ANIMATION_KEY_WALK = "Walk";

    private NavMeshAgent _navMeshAgent;
    private NavMeshAgentControllerAsset _navMeshAgentController;

    private Transform[] _exitWaypoints;

    private bool _isCustomerAlreadyVisitedTheOffice;

    #endregion

    #region Configuretion

    private void MoveCustomer(Transform[] destination, UnityAction OnDestinationReached) {

        animator.SetTrigger(ANIMATION_KEY_WALK);

        _navMeshAgentController.MoveAgent(_navMeshAgent, destination, OnDestinationReached);
    }


    #endregion

    #region Public Callback

    public void Initialization(NavMeshAgentControllerAsset navMeshAgentController, Transform[] officeWaypoints, Transform[] exitWaypoint) {

        _navMeshAgentController = navMeshAgentController;
        _exitWaypoints              = exitWaypoint;
        _isCustomerAlreadyVisitedTheOffice = false;

        MoveCustomer(
            officeWaypoints,
            delegate
            {
                gameManager.ChangeGameState(GameState.CustomerArrived);
            });

    }

    public void ExitFromOffice(bool hasAcceptedTheDeal) {

        if (hasAcceptedTheDeal)
        {
            switch (sharedData.currentCustomerType) {

                case CustomerType.Seller:

                    sharedData.flagForVisitedTheLandForDecoration = false;
                    gameManager.LoadScene(
                        sharedData.landAssetOnSeller.sceneForTheLand,
                        OnSceneLoaded : () =>
                        {
                            gameManager.ChangeGameState(GameState.HousePlacementStarted);
                        });

                    break;

                case CustomerType.Buyer:

                    MoveCustomer(_exitWaypoints, delegate
                    {
                        Destroy(gameObject);
                    });
                    gameManager.ChangeGameState(GameState.CustomerArriving);

                    break;
            }
        }
        else {

            MoveCustomer(_exitWaypoints, delegate
            {
                Destroy(gameObject);
            });
            gameManager.ChangeGameState(GameState.CustomerArriving);
        }


        
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
