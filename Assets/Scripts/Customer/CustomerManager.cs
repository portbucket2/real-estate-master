﻿using UnityEngine;
using System.Collections;
using com.faith.core;

public class CustomerManager : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

#if UNITY_EDITOR
        _customerIndex = 0;
#endif
        _transformReference = transform;
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnDayStarted()
    {
        int currentDay = sharedData.dayIndex;

        sharedData.numberOfLandSellerAgent = gameManager.levelData.listOfLevelData[currentDay].numberOfSellerLandAgent;
        sharedData.remainingNumberOfLandSellerAgent = sharedData.numberOfLandSellerAgent;

        sharedData.numberOfLandBuyerAgent = gameManager.levelData.listOfLevelData[currentDay].numberOfBuyerLandAgent;
        sharedData.remainingNumberOfLandBuyerAgent = sharedData.numberOfLandBuyerAgent;


        int valueToBeAdjust = sharedData.levelIndex % gameManager.levelData.maxNumberOfCustomerPerDay;
        CoreDebugger.Debug.Log("Value Adjusted : " + valueToBeAdjust, gameManager.configAssetForFeatureUnderDevelopment);
        for (int i = 0; i < valueToBeAdjust; i++) {

            int maxNumberOfRemainingAgent = Mathf.Max(sharedData.remainingNumberOfLandSellerAgent, sharedData.remainingNumberOfLandBuyerAgent);
            if (maxNumberOfRemainingAgent == sharedData.remainingNumberOfLandSellerAgent)
                sharedData.remainingNumberOfLandSellerAgent--;
            else
                sharedData.remainingNumberOfLandBuyerAgent--;
        }

        gameManager.ChangeGameState(GameState.CustomerArriving);
    }

    public override void OnCustomerArriving()
    {
        SpawnCustomer();
    }

    public override void OnLandNotPurchased()
    {
        base.OnLandNotPurchased();
    }

    public override void OnHousePlacementEnded()
    {
        base.OnHousePlacementEnded();
    }

    public override void OnDecorationEnded()
    {
        base.OnDecorationEnded();
    }

    public override void OnLandSold()
    {
        base.OnLandSold();
    }


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public NavMeshAgentControllerAsset navMeshController;
    public CustomerAsset customerAsset;


    [Space(5.0f)]
    public Transform spawnPoint;
    public Transform[] officeWaypointsPoint;
    public Transform[] exitWaypointsPoint;

    #endregion

    #region Private Variables

#if UNITY_EDITOR

    private int _customerIndex = 0;

#endif

    private Transform _transformReference;


    #endregion

    #region Configuretion

    private IEnumerator ChangeFlagWithDelay() {

        yield return new WaitForSeconds(0.1f);

        sharedData.flagForVisitedTheLandForDecoration = false;

        StopCoroutine(ChangeFlagWithDelay());
    }

    private void SpawnCustomer() {

        int customerIndex = -1;

        Transform _parent = _transformReference;
        Transform _spawnPoint       = spawnPoint;
        Transform[] _officeWaypoint   = officeWaypointsPoint;
        if (sharedData.flagForVisitedTheLandForDecoration)
        {
            customerIndex   = sharedData.customerIndex;

            _spawnPoint     = officeWaypointsPoint[officeWaypointsPoint.Length - 1];
            _officeWaypoint = new Transform[1] { _spawnPoint };
            _parent = _spawnPoint;

            StartCoroutine(ChangeFlagWithDelay());
        }
        else {

            if (sharedData.remainingNumberOfLandSellerAgent > 0 || sharedData.remainingNumberOfLandBuyerAgent > 0)
            {
                int maxNumberOfRemainingAgent = Mathf.Max(sharedData.remainingNumberOfLandSellerAgent, sharedData.remainingNumberOfLandBuyerAgent);
                if (maxNumberOfRemainingAgent == sharedData.remainingNumberOfLandSellerAgent)
                {
                    sharedData.currentCustomerType = CustomerType.Seller;
                    sharedData.landAssetOnSeller = gameManager.landAssets.GetALandAssetInfo(sharedData.levelIndex);
                    sharedData.remainingNumberOfLandSellerAgent--;
                }
                else
                {
                    sharedData.currentCustomerType = CustomerType.Buyer;
                    sharedData.landTypeFromBuyer = gameManager.landAssets.GetLandAssetTypeForBuyer(sharedData.levelIndex);
                    sharedData.remainingNumberOfLandBuyerAgent--;
                }
            }
            else {

                gameManager.ChangeGameState(GameState.DayEnd);
                return;
            }
        }
         

        GameObject newCustomer = customerAsset.Instanciate(
            ref sharedData.customerIndex,
            ref _parent,
            _spawnPoint.position, customerIndex : customerIndex);
#if UNITY_EDITOR
        newCustomer.name += "_ID(" + (_customerIndex++) + ")";
#endif
        newCustomer.GetComponent<CustomerController>().Initialization(
            navMeshController,
            _officeWaypoint,
            exitWaypointsPoint);
    }

#endregion

#endregion ALL SELF DECLEAR FUNCTIONS

}
