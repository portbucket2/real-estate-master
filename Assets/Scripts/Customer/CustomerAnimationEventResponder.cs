﻿using UnityEngine;

public class CustomerAnimationEventResponder : MonoBehaviour
{
    #region Public Variables

    public CustomerController customerControllerReference;

    #endregion

    #region Public Callback

    public void OnWinExpressionEnd() {

        customerControllerReference.ExitFromOffice(true);
    }

    public void OnRejectExpressionEnd() {

        customerControllerReference.ExitFromOffice(false);
    }

    #endregion
}
