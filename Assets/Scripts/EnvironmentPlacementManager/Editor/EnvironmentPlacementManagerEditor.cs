﻿using UnityEngine;
using UnityEditor;
using com.faith.core;

[CustomEditor(typeof(EnvironmentPlacementManager))]
public class EnvironmentPlacementManagerEditor : BaseEditorClass
{
    #region Private Variables

    private EnvironmentPlacementManager _reference;

    private SerializedProperty _landType;
    private SerializedProperty _intervalBetweenSpawningDecorationItem;
    private SerializedProperty _appearAnimator;
    private SerializedProperty _disappearAnimator;
    private SerializedProperty _parentForEnvironment;
    private SerializedProperty _decorationPlacement;


    #endregion

    #region Editor

    public override void OnEnable()
    {
        base.OnEnable();

        _reference = (EnvironmentPlacementManager)target;

        if (_reference == null)
            return;

        _landType = serializedObject.FindProperty("landType");

        _intervalBetweenSpawningDecorationItem = serializedObject.FindProperty("intervalBetweenSpawningDecorationItem");

        _appearAnimator = serializedObject.FindProperty("appearAnimator");
        _disappearAnimator = serializedObject.FindProperty("disappearAnimator");

        _parentForEnvironment = serializedObject.FindProperty("parentForEnvironment");

        _decorationPlacement = serializedObject.FindProperty("decorationPlacement");

        OnLandTypeAssigned();
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        ShowScriptReference();

        EditorGUILayout.PropertyField(_intervalBetweenSpawningDecorationItem);
        EditorGUILayout.PropertyField(_appearAnimator);
        EditorGUILayout.PropertyField(_disappearAnimator);
        EditorGUILayout.PropertyField(_parentForEnvironment);

        DrawHorizontalLine();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(_landType);
        if (EditorGUI.EndChangeCheck()) {

            OnLandTypeAssigned();
        }

        bool _isLandTypeAssigned = _landType.objectReferenceValue == null ? false : true;
        if (_isLandTypeAssigned)
        {
            DrawHorizontalLine();
            EditorGUILayout.LabelField("DecorationPlacement", EditorStyles.boldLabel);
            int numberOfDecorationPlacement = _decorationPlacement.arraySize;
            for (int i = 0; i < numberOfDecorationPlacement; i++) {

                SerializedProperty _isShowOnEditor = _decorationPlacement.GetArrayElementAtIndex(i).FindPropertyRelative("showInEditor");

                EditorGUI.indentLevel += 1;
                _isShowOnEditor.boolValue = EditorGUILayout.Foldout(
                        _isShowOnEditor.boolValue,
                        "Element (" + i + ")",
                        true
                    );
                if (_isShowOnEditor.boolValue) {

                    EditorGUI.indentLevel += 1;
                    EditorGUI.BeginDisabledGroup(true);
                    EditorGUILayout.PropertyField(_decorationPlacement.GetArrayElementAtIndex(i).FindPropertyRelative("decorationType"));
                    EditorGUI.EndDisabledGroup();

                    EditorGUILayout.PropertyField(_decorationPlacement.GetArrayElementAtIndex(i).FindPropertyRelative("locationInfos"));
                    EditorGUI.indentLevel -= 1;
                }
                EditorGUI.indentLevel -= 1;
            }
        }
        else {

            EditorGUILayout.HelpBox("Please assign 'LandType' in order to set their location of spawn", MessageType.Info);

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(_decorationPlacement);
            EditorGUI.EndDisabledGroup();
        }
        

        serializedObject.ApplyModifiedProperties();
    }

    #endregion

    #region Configuretion

    private void OnLandTypeAssigned() {

        _landType.serializedObject.ApplyModifiedProperties();

        if (_landType.objectReferenceValue == null)
        {
            _decorationPlacement.arraySize = 0;
            _decorationPlacement.serializedObject.ApplyModifiedProperties();
            return;
        }

        int numberOfHouseCanBePlaced = _reference.landType.houseTypeCanBePlaced.Length;
        int numberOfDecorationItem = _reference.landType.decorationType.Length;
        _decorationPlacement.arraySize = numberOfDecorationItem + 1;
        _decorationPlacement.serializedObject.ApplyModifiedProperties();
        for (int i = 0; i <= numberOfDecorationItem; i++)
        {

            SerializedProperty _decorationType = _decorationPlacement.GetArrayElementAtIndex(i).FindPropertyRelative("decorationType");

            if (i == 0)
            {
                _decorationType.arraySize = numberOfHouseCanBePlaced;
                _decorationType.serializedObject.ApplyModifiedProperties();

                for (int j = 0; j < numberOfHouseCanBePlaced; j++)
                {
                    _reference.decorationPlacement[i].decorationType[j] = _reference.landType.houseTypeCanBePlaced[j];
                }
            }
            else
            {

                _decorationType.arraySize = 1;
                _decorationType.serializedObject.ApplyModifiedProperties();

                _reference.decorationPlacement[i].decorationType[0] = _reference.landType.decorationType[i - 1];
            }

            _decorationType.serializedObject.ApplyModifiedProperties();
        }

        _decorationPlacement.serializedObject.ApplyModifiedProperties();
    }

    #endregion
}
