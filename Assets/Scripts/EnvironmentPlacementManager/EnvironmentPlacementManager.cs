﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faith.core;

public class EnvironmentPlacementManager : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

#if UNITY_EDITOR

#endif

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS



    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Custom Variables

    [System.Serializable]
    public class LocationInfo
    {
        public Transform location;
        public Vector3 initialLocalEulerAngle;
    }

    [System.Serializable]
    public class DecorationPlacementInfo
    {

#if UNITY_EDITOR
        [HideInInspector,SerializeField] private bool showInEditor;
#endif

        public EnumAssetForDecorationType[] decorationType;
        [Space(2.5f)]
        public LocationInfo[] locationInfos;
        
    }

    #endregion

    #region Public Variables

    public EnumAssetForLand landType;

    [Space(5.0f)]
    public FloatReference intervalBetweenSpawningDecorationItem;

    [Space(5.0f)]
    public TransformAnimatorAsset appearAnimator;
    public TransformAnimatorAsset disappearAnimator;

    [Space(5.0f)]
    public Transform parentForEnvironment;

    [Space(5.0f)]
    public DecorationPlacementInfo[]    decorationPlacement;


    #endregion

    #region Configuretion

    private IEnumerator ControllerForSpawningDecorationItem(EnumAssetForDecorationType decorationType, int decorationItemIndex, LocationInfo[] locationInfos, UnityAction OnPlacementEnd) {

        WaitForSeconds cycleDelay = new WaitForSeconds(intervalBetweenSpawningDecorationItem);

        int numberOfLocationInfo = locationInfos.Length;
        for (int i = 0; i < numberOfLocationInfo; i++) {

            Transform transformReferenceForNewDecorationItem = Instantiate(decorationType.decorationItems[decorationItemIndex].decorationPrefab, parentForEnvironment).transform;
            transformReferenceForNewDecorationItem.position = locationInfos[i].location.position;
            transformReferenceForNewDecorationItem.localEulerAngles = locationInfos[i].initialLocalEulerAngle;

            appearAnimator.DoAnimate(
                new List<Transform>() {
                    transformReferenceForNewDecorationItem
                },
                () => {
                    decorationType.SpawnParticle(ref transformReferenceForNewDecorationItem);
                });

            yield return cycleDelay;
        }

        StopCoroutine(ControllerForSpawningDecorationItem(null, -1, null, null));
        OnPlacementEnd?.Invoke();
    } 

    #endregion

    #region Public Callback

    public void PlaceDecorationItemInEnvironment(EnumAssetForDecorationType decorationType, int decorationItemIndex, UnityAction OnPlacementEnd = null) {

        bool hasFoundDecorationType = false;
        foreach (DecorationPlacementInfo decorationPlacementInfo in decorationPlacement) {

            int numberOfDecorationType = decorationPlacementInfo.decorationType.Length;
            for (int i = 0; i < numberOfDecorationType; i++) {

                if (decorationPlacementInfo.decorationType[i] == decorationType) {

                    hasFoundDecorationType = true;
                    StartCoroutine(ControllerForSpawningDecorationItem(decorationType, decorationItemIndex, decorationPlacementInfo.locationInfos, OnPlacementEnd));
                }
            }
        }

        if (!hasFoundDecorationType) {

            CoreDebugger.Debug.LogError("DecorationType missmatched from 'Land' to 'Scene'"
                + "\nRequestedDecorationType : " + decorationType.nameOfEnum);

            int numberOfDecorationType = decorationPlacement.Length;
            for (int i = 0; i < numberOfDecorationType; i++) {

                int numberOfDecoration = decorationPlacement[i].decorationType.Length;
                for (int j = 0; j < numberOfDecoration; j++) {

                    CoreDebugger.Debug.LogError("SceneDecorationType : (i = " + i + ",j = " + j + ") : " + decorationPlacement[i].decorationType[j].nameOfEnum);
                }
                
            }
        }
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
