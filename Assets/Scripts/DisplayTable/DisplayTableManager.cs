﻿using System.Collections.Generic;
using UnityEngine;
using com.faith.camera;
using com.faith.core;

public class DisplayTableManager : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    public override void Awake()
    {
        base.Awake();
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnSceneLoadInitialization()
    {
        base.OnSceneLoadInitialization();
        InitializationOfDisplayTable();
    }

    public override void OnEnterInInventory()
    {
        base.OnEnterInInventory();

        if (sharedData.shelfMode == ShelfMode.SellingToBuyer)
        {
            CoreDebugger.Debug.Log("DisplayInteractable : OnlyBuyer");
            MakeDisplayItemInteractableOnlyForBuyer();
        }
        else {

            CoreDebugger.Debug.Log("DisplayInteractable : All");
            MakeAllDisplayItemInteractable();
        }
    }

    public override void OnExitFromInventory()
    {
        base.OnExitFromInventory();

        particleForSelection.Stop();
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public FaithCameraSettings cameraSettingForFocusingShelf;

    [Space(5.0f)]
    public ParticleSystem particleForSelection;

    [Space(5.0f)]
    public GameObject prefabForDisplayTable;
    public GameObject prefabForDisplayItem;

    [Space(5.0f)]
    public Transform parentOfDisplayTable;
    public Vector3 offsetBetweenDisplayTable;
    [Range(0.1f, 1f)]
    public float slidingSpeed = 0.1f;

    #endregion

    #region Private Variables

    private List<DisplayTable> _listOfDisplayTable;

    #endregion

    #region Configuretion

    private void InitializationOfDisplayTable()
    {
        _listOfDisplayTable = new List<DisplayTable>();

        int numberOfAvailableDisplayTable = gameManager.inventory.NumberOfAvailableInventoryStack;
        for (int i = 0; i < numberOfAvailableDisplayTable; i++)
        {
            AddNewDisplayTable(i);
        }

        PopulatingDisplayTable();
    }

    private void MakeAllDisplayItemInteractable() {

        int numberOfAvailableDisplayTable = gameManager.inventory.NumberOfAvailableInventoryStack;
        for (int i = 0; i < numberOfAvailableDisplayTable; i++)
        {

            int numberOfItemInDisplayTable = gameManager.inventory.NumberOfItemInDisplayTable(i);
            for (int j = 0; j < numberOfItemInDisplayTable; j++)
            {
                _listOfDisplayTable[i].SetInteractableFlag(j, true, gameManager.inventory.IsHouseDecorated(i,j));
            }
        }
    }

    private void MakeDisplayItemInteractableOnlyForBuyer() {

        int numberOfAvailableDisplayTable = gameManager.inventory.NumberOfAvailableInventoryStack;
        for (int i = 0; i < numberOfAvailableDisplayTable; i++)
        {

            int numberOfItemInDisplayTable = gameManager.inventory.NumberOfItemInDisplayTable(i);
            for (int j = 0; j < numberOfItemInDisplayTable; j++)
            {
                int landUniqueID = gameManager.inventory.GetLandUniqueID(i, j);
                if (sharedData.landTypeFromBuyer == gameManager.landAssets.listOfLandAssetInfo[landUniqueID].landType)
                {
                    _listOfDisplayTable[i].SetInteractableFlag(j, true, gameManager.inventory.IsHouseDecorated(i, j));
                }
                else {

                    _listOfDisplayTable[i].SetInteractableFlag(j, false, gameManager.inventory.IsHouseDecorated(i, j));
                }
            }
        }
    }

    #endregion

    #region Public Callback

    

    public void AddNewDisplayTable(int displayTableIndex = -1) {

        DisplayTable newDisplayTable = Instantiate(prefabForDisplayTable, parentOfDisplayTable).GetComponent<DisplayTable>();
        newDisplayTable.transform.localPosition = (displayTableIndex == -1 ? gameManager.inventory.NumberOfAvailableInventoryStack : displayTableIndex) * offsetBetweenDisplayTable;

        _listOfDisplayTable.Add(newDisplayTable);
    }

    public void PopulatingDisplayTable()
    {
        particleForSelection.transform.SetParent(transform);
        particleForSelection.Stop();

        int numberOfAvailableDisplayTable = gameManager.inventory.NumberOfAvailableInventoryStack;
        for (int i = 0; i < numberOfAvailableDisplayTable; i++)
        {

            _listOfDisplayTable[i].ClearAllDisplayItem();
            int numberOfItemInDisplayTable = gameManager.inventory.NumberOfItemInDisplayTable(i);
            for (int j = 0; j < numberOfItemInDisplayTable; j++)
            {

                AddNewItemInDisplayTable(i, j);
            }
        }

        MakeAllDisplayItemInteractable();
    }

    public void AddNewItemInDisplayTable(int displayTableIndex = -1, int displayItemIndex = -1) {

        displayTableIndex   = displayTableIndex == -1 ? gameManager.inventory.NumberOfAvailableInventoryStack : displayTableIndex;
        displayItemIndex    = displayItemIndex == -1 ? gameManager.inventory.NumberOfItemInDisplayTable(displayTableIndex) : displayItemIndex;

        GameObject newDisplayItem   = Instantiate(prefabForDisplayItem, _listOfDisplayTable[displayTableIndex].GetTransformOfDisplayPosition(displayItemIndex));
        
        int landUniqueId            = gameManager.inventory.GetLandUniqueID(displayTableIndex, displayItemIndex);
        GameObject newLandDisplay   = Instantiate(gameManager.landAssets.listOfLandAssetInfo[landUniqueId].landDisplayPrefab, _listOfDisplayTable[displayTableIndex].GetTransformOfDisplayPosition(displayItemIndex));

        int houseTypeIndex          = gameManager.inventory.GetHouseTypeIndex(displayTableIndex, displayItemIndex);
        int houseIndex              = gameManager.inventory.GetHouseIndex(displayTableIndex, displayItemIndex);
        GameObject newHouseDisplay  = Instantiate(gameManager.landAssets.listOfLandAssetInfo[landUniqueId].houseTypeCanBePlaced[houseTypeIndex].decorationItems[houseIndex].decorationPrefab, _listOfDisplayTable[displayTableIndex].GetTransformOfDisplayPosition(displayItemIndex));

        DisplayItem displayItem = newDisplayItem.GetComponent<DisplayItem>();
        displayItem.PassInventoryParameter(displayTableIndex, displayItemIndex);
        displayItem.PlaceLandDisplay(newLandDisplay.transform);
        displayItem.PlaceHouseDisplay(newHouseDisplay.transform);
        _listOfDisplayTable[displayTableIndex].PlaceDisplayItem(displayItemIndex, displayItem);
    }

    public void MoveDisplayTable(Vector3 value) {

        Vector3 newLocalPosition = parentOfDisplayTable.localPosition + value;
        parentOfDisplayTable.localPosition = Vector3.Lerp(parentOfDisplayTable.localPosition,newLocalPosition,slidingSpeed);
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
