﻿using UnityEngine;
public class DisplayItem : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public Transform landDisplayPosition;
    public Transform houseDisplayPosition;

    [Header("Parameter  :   HitBox")]
    public Collider hitBoxForSelection;
    [Range(0.1f,1f)]
    public float downScaledWhenNonSelectable = 0.1f;

    [Header("Parameter  :   VisualFX")]
    public ParticleSystem particleForDecorated;

    public int displayTableIndex { get; private set; }
    public int displayItemIndex { get; private set; }

    #endregion

    #region Private Variables

    

    #endregion

    #region Public Callback

    public void PassInventoryParameter(int displayTableIndex, int displayItemIndex) {

        this.displayTableIndex  = displayTableIndex;
        this.displayItemIndex   = displayItemIndex;
    }

    public void PlaceLandDisplay(Transform landDisplay) {

        landDisplay.SetParent(landDisplayPosition);
        landDisplay.localPosition = Vector3.zero;
        landDisplay.localScale = Vector3.one;
    }

    public void PlaceHouseDisplay(Transform houseDisplay) {

        houseDisplay.SetParent(houseDisplayPosition);
        houseDisplay.localPosition = Vector3.zero;
        houseDisplay.localScale = Vector3.one;
    }

    public void MarkAsSelected(ref ParticleSystem _particleForSelection) {
        _particleForSelection.transform.SetParent(landDisplayPosition);
        _particleForSelection.transform.localPosition = new Vector3(-0.038f,0,0.205f);
        _particleForSelection.Play();
    }

    public void SetInteractableFlag(bool flag, bool isDecorated) {

        hitBoxForSelection.enabled  = flag;
        transform.localScale        = flag ? Vector3.one : Vector3.one * downScaledWhenNonSelectable;

        int landUniqueID = gameManager.inventory.GetLandUniqueID(displayTableIndex, displayItemIndex);
        int houseTypeIndex = gameManager.inventory.GetHouseTypeIndex(displayTableIndex, displayItemIndex);
        int houseIndex = gameManager.inventory.GetHouseIndex(displayTableIndex, displayItemIndex);

        MeshRenderer[] meshRendererReferencesForHouse = houseDisplayPosition.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer meshRenderer in meshRendererReferencesForHouse) {

            meshRenderer.material = flag? gameManager.landAssets.listOfLandAssetInfo[landUniqueID].houseTypeCanBePlaced[houseTypeIndex].decorationItems[houseIndex].defaultMaterial : gameManager.landAssets.listOfLandAssetInfo[landUniqueID].houseTypeCanBePlaced[houseTypeIndex].decorationItems[houseIndex].transaculateMaterial;
        }

        MeshRenderer[] meshRendererReferencesForLandCube = landDisplayPosition.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer meshRenderer in meshRendererReferencesForLandCube)
        {

            meshRenderer.material = flag ? gameManager.landAssets.listOfLandAssetInfo[landUniqueID].defaultMaterialForLandDisplay : gameManager.landAssets.listOfLandAssetInfo[landUniqueID].transaculateMaterialForLandDisplay;
        }

        if (flag && isDecorated)
            particleForDecorated.Play();
        else
            particleForDecorated.Stop();
    }


    public void Destroy() {

        Destroy(gameObject);
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
