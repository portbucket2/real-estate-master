﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using com.faith.core;

[CustomEditor(typeof(DisplayTable))]
public class DisplayTableEditor : BaseEditorClass
{
    #region Private Variables

    private DisplayTable _reference;

    private SerializedProperty _sp_numberOfSlotPerTable;
    private SerializedProperty _sp_arrayOfDisplaySlot;

    private bool _isIntReferenceForSlotPerTableAssigned = false;

    #endregion

    #region Editor

    public override void OnEnable()
    {
        base.OnEnable();

        _reference = (DisplayTable)target;

        if (_reference == null)
            return;

        _sp_numberOfSlotPerTable    = serializedObject.FindProperty("numberOfSlotPerTable");
        _sp_arrayOfDisplaySlot       = serializedObject.FindProperty("_arrayOfDisplaySlot");
    }

    public override void OnInspectorGUI()
    {
        ShowScriptReference();

        serializedObject.Update();

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(_sp_numberOfSlotPerTable);
        if (EditorGUI.EndChangeCheck()) {

            _sp_numberOfSlotPerTable.serializedObject.ApplyModifiedProperties();

            if (!_reference.numberOfSlotPerTable.UseConstant) {

                if (_reference.numberOfSlotPerTable.Variable == null)
                {
                    _sp_arrayOfDisplaySlot.arraySize = 0;
                    _sp_arrayOfDisplaySlot.serializedObject.ApplyModifiedProperties();
                    return;
                }
            }

            _sp_arrayOfDisplaySlot.arraySize = _reference.numberOfSlotPerTable.Value;
            _sp_arrayOfDisplaySlot.serializedObject.ApplyModifiedProperties();

            _isIntReferenceForSlotPerTableAssigned = true;
        }

        if (_sp_arrayOfDisplaySlot.arraySize > 0) {

            DrawHorizontalLine();
            EditorGUILayout.LabelField("ListOfDisplaySlot", EditorStyles.boldLabel);
            EditorGUI.indentLevel += 1;
            for (int i = 0; i < _reference.numberOfSlotPerTable; i++) {

                SerializedProperty _showSlot = _sp_arrayOfDisplaySlot.GetArrayElementAtIndex(i).FindPropertyRelative("_showDisplaySlotParameter");

                _showSlot.boolValue = EditorGUILayout.Foldout(
                        _showSlot.boolValue,
                        "DisplaySlot(" + i + ")",
                        true
                    );

                if (_showSlot.boolValue) {

                    EditorGUI.indentLevel += 1;

                    EditorGUILayout.PropertyField(_sp_arrayOfDisplaySlot.GetArrayElementAtIndex(i).FindPropertyRelative("displayPosition"));

                    EditorGUI.BeginDisabledGroup(true);
                    EditorGUILayout.PropertyField(_sp_arrayOfDisplaySlot.GetArrayElementAtIndex(i).FindPropertyRelative("displayItem"));
                    EditorGUI.EndDisabledGroup();

                    EditorGUI.indentLevel -= 1;
                }
            }
            EditorGUI.indentLevel -= 1;
        }

        serializedObject.ApplyModifiedProperties();
    }

    #endregion
}
