﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faith.core;

public class DisplayTable : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        IndexForCurrentEmptyDisplaySlot = 0;
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS


    #region Custom Variables

    [System.Serializable]
    private class DisplaySlot
    {
#if UNITY_EDITOR

        [SerializeField] private bool _showDisplaySlotParameter;

        #endif

        public Transform    displayPosition;
        public DisplayItem  displayItem;
    }

    #endregion

    #region Public Variables

    public int IndexForCurrentEmptyDisplaySlot { get; private set; }

    public IntReference numberOfSlotPerTable;


    #endregion

    #region Private Variables

    [SerializeField] private DisplaySlot[] _arrayOfDisplaySlot;

    #endregion

    #region Public Callback

    public Transform GetTransformOfDisplayPosition(int displaySlotIndex) {

        return _arrayOfDisplaySlot[displaySlotIndex].displayPosition;
    }

    public void PlaceDisplayItem(int displaySlotIndex, DisplayItem displayItem) {

        Transform transformReference = displayItem.transform;
        transformReference.SetParent(_arrayOfDisplaySlot[displaySlotIndex].displayPosition);
        transformReference.localPosition = Vector3.zero;

        _arrayOfDisplaySlot[displaySlotIndex].displayItem = displayItem;
    }

    public void SetInteractableFlag(int displaySlotIndex, bool flag, bool isDecorated)
    {
        if(_arrayOfDisplaySlot[displaySlotIndex].displayItem != null)
            _arrayOfDisplaySlot[displaySlotIndex].displayItem.SetInteractableFlag(flag, isDecorated);
    }

    public void ClearAllDisplayItem() {

        for (int i = 0; i < numberOfSlotPerTable; i++) {

            ClearDisplayItem(i);
        }
    }

    public void ClearDisplayItem(int displaySlotIndex)
    {
        if (_arrayOfDisplaySlot[displaySlotIndex].displayItem != null) {

            _arrayOfDisplaySlot[displaySlotIndex].displayItem.Destroy();
            _arrayOfDisplaySlot[displaySlotIndex].displayItem = null;
        }
        
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
