﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.faith.core;
public class DoorAnimationController : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        
    }

    public override void OnEnable()
    {
        base.OnEnable();

        _doorTransformRefernce = transform;
    }

    public override void OnCustomerArriving()
    {
        base.OnCustomerArriving();

        if(!sharedData.flagForVisitedTheLandForDecoration)
            animatorForOpening.DoAnimate(new List<Transform>() { _doorTransformRefernce });
    }

    public override void OnCustomerArrived()
    {
        base.OnCustomerArrived();

        if (!sharedData.flagForVisitedTheLandForDecoration)
            animatorForClosing.DoAnimate(new List<Transform>() { _doorTransformRefernce });
    }

    public override void OnDayEnded()
    {
        base.OnDayEnded();

        StartCoroutine(DoorCloseDelayOnDayEnd());
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public TransformAnimatorAsset animatorForOpening;
    public TransformAnimatorAsset animatorForClosing;

    #endregion

    #region Private Variables

    private static Transform    _doorTransformRefernce;

    #endregion

    #region Configuretion

    private IEnumerator DoorCloseDelayOnDayEnd() {

        yield return new WaitForSeconds(1);

        animatorForClosing.DoAnimate(new List<Transform>() { _doorTransformRefernce });

        StopCoroutine(DoorCloseDelayOnDayEnd());
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
