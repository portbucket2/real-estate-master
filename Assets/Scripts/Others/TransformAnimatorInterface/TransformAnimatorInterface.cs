﻿
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using com.faith.core;

public class TransformAnimatorInterface : MonoBehaviour
{
    #region Custom Variables

    [System.Serializable]
    public class TransformAnimationClipProperty
    {
        [Range(0f,5f)]
        public float initialDelay = 0;
        public TransformAnimatorAsset animatorAsset;
        public List<Transform> listOfAnimatedTransform;
    }

    [System.Serializable]
    public class TransformAnimationClip
    {
        public string clipUniqueID;
        public TransformAnimationClipProperty[] clipVarients;
    }

    #endregion

    #region Public Variables

#if UNITY_EDITOR

    [HideInInspector] public int previewClipIndex = 0;

#endif

    public TransformAnimationClip[] animationClip;

    #endregion

    #region Private Variables

    private bool _isTransformAnimatorRunning = false;
    private bool _isForceQuitExecuted = false;
    private int _numberOfAnimationClip = 0;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        _numberOfAnimationClip = animationClip.Length;
    }

    #endregion

    #region Configuretion

    private bool IsValidClipIndex(int clipIndex) {

        if (clipIndex >= 0 && clipIndex < animationClip.Length)
            return true;

        CoreDebugger.Debug.LogError("Invalid ClipIndex : " + clipIndex);
        return false;
    }

    private int IsValidClipUniqueID(string clipUniqueID) {

        for (int i = 0; i < _numberOfAnimationClip; i++) {

            if (StringOperation.IsSameString(clipUniqueID, animationClip[i].clipUniqueID))
                return i;
        }

        CoreDebugger.Debug.LogError("Invalid ClipUniqueId : " + clipUniqueID);
        return -1;
    }

    private float GetHighestDelay(ref TransformAnimationClip animationClip) {

        float highestDelay = 0;

        foreach (TransformAnimationClipProperty clipProperty in animationClip.clipVarients) {

            highestDelay = Mathf.Max(highestDelay, clipProperty.initialDelay);
        }

        return highestDelay;
    }

    private IEnumerator ControllerForClipSequence(int clipIndex, UnityAction OnAnimationEnd) {

        int numberOfClip = animationClip[clipIndex].clipVarients.Length;

        bool[] isAnimationTriggerd = new bool[numberOfClip];

        float heighestDelay = GetHighestDelay(ref animationClip[clipIndex]);
        float timeCounter = 0;
        float cycleLength = 0.0167f;

        WaitForSeconds cycleDelay = new WaitForSeconds(cycleLength);

        while (!_isForceQuitExecuted) {

            for (int i = 0; i < numberOfClip; i++) {

                if (!isAnimationTriggerd[i] && timeCounter > animationClip[clipIndex].clipVarients[i].initialDelay) {

                    bool isLastAnimationClip = (i == (numberOfClip - 1)? true : false);

                    isAnimationTriggerd[i] = true;

                    animationClip[clipIndex].clipVarients[i].animatorAsset.DoAnimate(
                        animationClip[clipIndex].clipVarients[i].listOfAnimatedTransform,
                        isLastAnimationClip ? OnAnimationEnd : null);

                    if (isLastAnimationClip)
                    {
                        _isForceQuitExecuted = true;
                        break;
                    }
                }
            }

            yield return cycleDelay;
            timeCounter += cycleLength;
        }

        StopCoroutine(ControllerForClipSequence(-1, null));
        _isTransformAnimatorRunning = false;
    }

    #endregion

    #region Public Callback

#if UNITY_EDITOR

    public void GoToNextClip() {

        previewClipIndex++;
        if (previewClipIndex >= animationClip.Length)
            previewClipIndex = 0;

    }

    public void GoToPreviousClip() {

        previewClipIndex--;
        if (previewClipIndex <= -1)
            previewClipIndex = animationClip.Length - 1;
    }

#endif

    public void PlayAnimationClip(string clipUniqueID, UnityAction OnAnimationEnd = null) {

        int clipIndex = IsValidClipUniqueID(clipUniqueID);
       
        if(clipIndex != -1) PlayAnimationClip(clipIndex);
    }

    public void PlayAnimationClip(int clipIndex, UnityAction OnAnimationEnd = null) {

        if (IsValidClipIndex(clipIndex) && !_isTransformAnimatorRunning) {

            _isTransformAnimatorRunning = true;
            _isForceQuitExecuted = false;
            StartCoroutine(ControllerForClipSequence(clipIndex, OnAnimationEnd));
        }
    }

    #endregion
}
