﻿using UnityEngine;
using UnityEditor;
using com.faith.core;

[CustomEditor(typeof(TransformAnimatorInterface))]
public class TransformAnimatorInterfaceEditor : BaseEditorClass
{
    #region Private Variables

    private TransformAnimatorInterface _reference;

    #endregion

    #region Editor

    public override void OnEnable()
    {
        _reference = (TransformAnimatorInterface)target;

        if (_reference == null)
            return;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        CustomGUI();
        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

    #endregion

    #region Custom GUI

    private void CustomGUI() {

        EditorGUILayout.Space();
        if (EditorApplication.isPlaying && _reference.animationClip.Length > 0) {

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Clip(" + _reference.previewClipIndex + ") : " + _reference.animationClip[_reference.previewClipIndex].clipUniqueID);
                if (GUILayout.Button("<", GUILayout.Width(25)))
                {

                    _reference.GoToPreviousClip();
                }
                if (GUILayout.Button("Animate", GUILayout.Width(100)))
                {
                    _reference.PlayAnimationClip(_reference.previewClipIndex);
                }
                if (GUILayout.Button(">", GUILayout.Width(25)))
                {
                    _reference.GoToNextClip();
                }
            }
            EditorGUILayout.EndHorizontal();
            DrawHorizontalLine();
        }
        
    }

    #endregion
}
