﻿using UnityEngine;
using System.Collections.Generic;
using TMPro;
using com.faith.core;


[RequireComponent(typeof(AccountManager))]
public class UIAccountDisplay : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    public void Start()
    {
        accountManager.OnBalanceChangeEvent(OnBalanceChanging, OnBalanceChangeEnd);   
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnDayStarted()
    {
        base.OnDayStarted();

        sharedData.totalMoneyEarned = 0;
        sharedData.totalMoneySpent = 0;
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public AccountManager   accountManager;

    [Space(5.0f)]
    public TextMeshProUGUI  cashText;

    [Header("Parameter  :   NoMoney")]
    [Range(0,10000000)]
    public double amountOfCashToBeAwardedWhenNoMoney = 1000;
    public PopUpPanelInfo popUpPanelInfoForNoCash;

    [Header("Parameter  :   Particles/FX")]
    public ParticleSystem particleForAddBalance;
    public ParticleSystem particleForDeductBalance;

    #endregion

    #region Configuretion

    private void UpdateCashDisplay() {

        cashText.text = accountManager.GetCurrentBalance().ToString("F0");
    }

    private void OnBalanceChanging(double value, CoreEnums.ValueChangedState valueChangedState) {

        UpdateCashDisplay();
    }

    private void OnBalanceChangeEnd(double value, CoreEnums.ValueChangedState valueChangedState) {

        switch (valueChangedState)
        {
            case CoreEnums.ValueChangedState.VALUE_INCREASED:
                particleForAddBalance.Play();
                break;

            case CoreEnums.ValueChangedState.VALUE_DECREASED:
                particleForDeductBalance.Play();
                break;
        }
    }

    #endregion

    #region Public Callback

    public void AddBalance(double amount, AccountManagerCurrencyEnum currencyEnum = AccountManagerCurrencyEnum.DEFAULT) {

        sharedData.totalMoneyEarned += amount;
        accountManager.AddBalance(amount, currencyEnum);
    }

    public bool DeductBalance(double amount, AccountManagerCurrencyEnum currencyEnum = AccountManagerCurrencyEnum.DEFAULT) {

        if (accountManager.DeductBalance(amount, currencyEnum))
        {
            sharedData.totalMoneySpent += amount;
            return true;
        }

        gameManager.popUpPanel.ShowPopUp(
            popUpPanelInfoForNoCash,
            "Free " + amountOfCashToBeAwardedWhenNoMoney.ToString("F0") + "$",
            () =>
            {
                AddBalance(amountOfCashToBeAwardedWhenNoMoney);
            });

        return false;
    }


    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
