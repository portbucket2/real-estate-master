﻿using UnityEngine;
using TMPro;

public class UITextBubbleForLandSellerCustomer : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnCustomerArrived()
    {
        if (sharedData.currentCustomerType == CustomerType.Seller) {

            landPriceField.text = sharedData.landAssetOnSeller.sellingPrice.ToString() + "$";

            animatorForTextBubble.SetTrigger("APPEAR");
        }
    }

    public override void OnLandNotPurchased()
    {
        animatorForTextBubble.SetTrigger("DISAPPEAR");
    }

    public override void OnLandPurchased()
    {
        animatorForTextBubble.SetTrigger("DISAPPEAR");
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public Animator animatorForTextBubble;

    [Space(2.5f)]
    public TextMeshProUGUI landPriceField;

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
