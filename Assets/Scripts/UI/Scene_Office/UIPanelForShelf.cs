﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using com.faith.core;

public class UIPanelForShelf : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    public override void Awake()
    {
        base.Awake();


        buttonForClosing.onClick.AddListener(() =>
        {
            UIInteract(buttonForClosing.transform, () =>
            {
                gameManager.ChangeGameState(GameState.ExitFromInventory);
            });
        });
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnEnterInInventory()
    {
        base.OnEnterInInventory();

        SetContainerVisibility(true);

        switch (sharedData.shelfMode) {

            case ShelfMode.SellingToBuyer:

                textForLandTypeAskingByBuyer.text   = sharedData.landTypeFromBuyer.nameOfEnum;
                textForOfferingPriceByBuyer.text    = gameManager.GetTheBuyingPriceForBuyer().ToString("F0");

                imageForLandIconAskingByBuyer.sprite= sharedData.landTypeFromBuyer.iconForEnum;

                UIAppear(buyerInfoPanel);

                if (gameManager.IsAnyLandAvailableForBuyer())
                    textMessageIfNoItemSelected.SetActive(true);
                else
                    textMessageIfNothingCanBeSold.SetActive(true);



                break;
            default:

                textMessageIfNoItemSelected.SetActive(true);

                break;
        }


        
    }

    public override void OnExitFromInventory()
    {
        base.OnExitFromInventory();

        SetContainerVisibility(false);
    }


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS


    #region Public Variables

    public GameObject container;


    [Header("Panel  :   BuyerInfoPanel")]
    public Transform        buyerInfoPanel;

    [Space(2.5f)]
    public TextMeshProUGUI textForLandTypeAskingByBuyer;
    public TextMeshProUGUI textForOfferingPriceByBuyer;

    [Space(2.5f)]
    public Image            imageForLandIconAskingByBuyer;

    [Header("Panel  :   SelectedItemPanel")]
    public Transform        selectedItemPanel;

    [Space(2.5f)]
    public GameObject textMessageIfNoItemSelected;
    public GameObject textMessageIfNothingCanBeSold;

    [Space(2.5f)]
    public TextMeshProUGUI  textForPurchasedPrice;
    public TextMeshProUGUI  textForEstimatedPrice;
    public TextMeshProUGUI  textForLandType;
    public TextMeshProUGUI  textForIsDecorated;
    public TextMeshProUGUI  textForSellButton;

    [Space(5.0f)]
    public Image            imageForLandType;

    [Space(2.5f)]
    public Button buttonForSell;
    public Button buttonForDecoration;
    public Button buttonForClosing;

    #endregion

    #region Private Variables

    private bool _isItemSelected;

    #endregion

    #region Configuretion

    private void SetContainerVisibility(bool flag)
    {
        _isItemSelected = false;

        selectedItemPanel.localScale    = Vector3.zero;
        buyerInfoPanel.localScale       = Vector3.zero;

        textMessageIfNoItemSelected.SetActive(false);
        textMessageIfNothingCanBeSold.SetActive(false);

        container.SetActive(flag);

    }

    #endregion

    #region Public Callback

    public void ShowDisplayItemData(int displayTableIndex, int displayItemIndex, UnityAction<int,int> OnLandQuickSell, UnityAction<int, int> OnLandSellToBuyer) {

        int landIndex               = gameManager.inventory.GetLandUniqueID(displayTableIndex, displayItemIndex);
        int houseTypeIndex          = gameManager.inventory.GetHouseTypeIndex(displayTableIndex, displayItemIndex);
        int houseIndex              = gameManager.inventory.GetHouseIndex(displayTableIndex, displayItemIndex);
        float estimatedPrice        = gameManager.landAssets.listOfLandAssetInfo[landIndex].estimatedPrice;
        textForPurchasedPrice.text  = "$" + gameManager.landAssets.listOfLandAssetInfo[landIndex].sellingPrice;
        textForEstimatedPrice.text  = "$" + gameManager.GetEstimatedPriceForLand(landIndex, displayTableIndex, displayItemIndex);
        textForLandType.text        = gameManager.landAssets.listOfLandAssetInfo[landIndex].landType.nameOfEnum;
        textForSellButton.text      = sharedData.currentCustomerType == CustomerType.Seller? "QuickSell" : "Sell";

        imageForLandType.sprite     = gameManager.landAssets.listOfLandAssetInfo[landIndex].landType.iconForEnum;

        textForIsDecorated.gameObject.SetActive(gameManager.inventory.IsHouseDecorated(displayTableIndex, displayItemIndex));

        ///Button   :   Sell
        buttonForSell.onClick.RemoveAllListeners();
        buttonForSell.onClick.AddListener(() =>
        {


            selectedItemPanel.localScale = Vector3.zero;
            _isItemSelected = false;

            if (sharedData.currentCustomerType == CustomerType.Seller)
            {

                gameManager.accountDisplay.AddBalance(estimatedPrice);
                OnLandQuickSell?.Invoke(displayTableIndex, displayItemIndex);
            }
            else {

                CoreDebugger.Debug.LogWarning("###Feature Need To Be Applied For Selling To Buyer");
                OnLandSellToBuyer?.Invoke(displayTableIndex, displayItemIndex);
            }

            
        });


        //Button    :   Decoration
        if (!gameManager.inventory.IsHouseDecorated(displayTableIndex, displayItemIndex))
        {

            buttonForDecoration.gameObject.SetActive(true);

            buttonForDecoration.onClick.RemoveAllListeners();
            buttonForDecoration.onClick.AddListener(() =>
            {
                sharedData.flagForVisitedTheLandForDecoration = false;
                gameManager.LoadScene(
                    gameManager.landAssets.listOfLandAssetInfo[landIndex].sceneForTheLand,
                    OnSceneLoaded: () =>
                    {
                        gameManager.ChangeGameState(GameState.DecorationStarted);
                    }
                );
            });
        }
        else buttonForDecoration.gameObject.SetActive(false);


        if (!_isItemSelected) {

            textMessageIfNoItemSelected.SetActive(false);
            UIAppear(selectedItemPanel);
            
            _isItemSelected = true;
        }
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
