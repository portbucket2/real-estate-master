﻿using UnityEngine;
using UnityEngine.UI;

public class UIManagerForOffice : UIBaseManager
{
    #region ALL UNITY FUNCTIONS

    public override void Awake()
    {
        base.Awake();

        startButton.onClick.AddListener(() => 
        {
            startPanel.SetActive(false);
            GameManager.Instance.ChangeGameState(GameState.DayStarted);
        });

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnDataLoaded()
    {
        base.OnDataLoaded();
        startPanel.SetActive(true);
    }

    public override void OnCustomerArrived()
    {
        base.OnCustomerArrived();

        if (sharedData.currentCustomerType == CustomerType.Buyer) {

            SetInputPanelVisibility(true);
        }
    }

    public override void OnAnalyzingComplete()
    {
        base.OnAnalyzingComplete();

        SetInputPanelVisibility(true);

    }

    public override void OnLandNotPurchased()
    {
        base.OnLandNotPurchased();

        SetInputPanelVisibility(false);
    }

    public override void OnLandPurchased()
    {
        base.OnLandPurchased();

        SetInputPanelVisibility(false);
    }

    public override void OnLandNotSold()
    {
        base.OnLandNotSold();

        SetInputPanelVisibility(false);
    }

    public override void OnLandSold()
    {
        base.OnLandSold();

        SetInputPanelVisibility(false);
    }

    public override void OnEnterInInventory()
    {
        base.OnEnterInInventory();

        SetInputPanelVisibility(false);
    }

    public override void OnExitFromInventory()
    {
        base.OnExitFromInventory();

        switch (sharedData.currentCustomerType) {
            case CustomerType.Seller:

                if (sharedData.isAnalyzingCompleteForSellerCustomer) {

                    SetInputPanelVisibility(true);
                }

                break;

            case CustomerType.Buyer:

                SetInputPanelVisibility(true);

                break;
        }
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    [Header("Panel  :   Start")]
    public GameObject   startPanel;
    public Button       startButton;

    [Header("Panel  :   Input")]
    [Space(5.0f)]
    public GameObject   inputPanel;

    #endregion

    #region Configuretion

    private void SetInputPanelVisibility(bool flag) {

        if (flag)
        {

            if (!gameManager.IsInputSystemInterrupted) inputPanel.SetActive(true);
            else
            {

                WaitUntilInterruptionRevoked(() =>
                {
                    inputPanel.SetActive(true);
                });
            }
        }
        else {

            inputPanel.SetActive(false);
        }
        
    }

    #endregion


    #endregion ALL SELF DECLEAR FUNCTIONS

}
