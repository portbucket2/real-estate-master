﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com.faith.core;

public class UIPanelForGameplay : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        shelfButton.onClick.AddListener(() =>
        {
            if (IsUIPanelWillBeInteractable()) {

                SetContainerVisibility(false);

                switch (sharedData.currentCustomerType)
                {
                    case CustomerType.Seller:
                        sharedData.shelfMode = ShelfMode.Browsing;
                        break;
                    case CustomerType.Buyer:
                        sharedData.shelfMode = ShelfMode.Browsing;
                        break;
                }

                gameManager.ChangeGameState(GameState.EnterInInventory);
            }
        });
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    public override void OnDayStarted()
    {
        base.OnDayStarted();
        SetContainerVisibility(true);
    }

    public override void OnCustomerArriving()
    {
        base.OnCustomerArriving();
        SetContainerVisibility(true);
    }

    public override void OnDayEnded()
    {
        base.OnDayEnded();
        SetContainerVisibility(false);
    }

    public override void OnEnterInInventory()
    {
        base.OnEnterInInventory();
        SetContainerVisibility(false);
    }

    public override void OnExitFromInventory()
    {
        base.OnExitFromInventory();
        SetContainerVisibility(true);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public GameObject container;

    [Space(5.0f)]
    public Button shelfButton;

    #endregion

    #region Configuretion

    private bool IsUIPanelWillBeInteractable() {

        if (gameManager.CurrentGameState == GameState.CustomerArriving
            || gameManager.CurrentGameState == GameState.CustomerArrived
            || gameManager.CurrentGameState == GameState.AnalyzingComplete
            || gameManager.CurrentGameState == GameState.ExitFromInventory)
        {
            switch (sharedData.currentCustomerType)
            {
                case CustomerType.Seller:
                    return true;
                case CustomerType.Buyer:

                    return false;
            }
        }

        return false;
    }

    private void SetContainerVisibility(bool flag)
    {
        container.SetActive(flag);
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
