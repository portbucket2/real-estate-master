﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using com.faith.core;
using TMPro;

public class UIDayProgressionTracking : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnCustomerArriving()
    {
        base.OnCustomerArriving();

        if (!gameObject.activeInHierarchy)
        {

            gameObject.SetActive(true);
            StartCoroutine(UpdateRemainingCustomerWithDelay());
            appearAnimator.DoAnimate(new System.Collections.Generic.List<Transform>() { transform });
        }
        else {

            StartCoroutine(UpdateRemainingCustomerWithDelay());
        }
    }

    public override void OnLandNotPurchased()
    {
        base.OnLandNotPurchased();

        gameManager.levelData.GoToNextLevel();
    }

    public override void OnLandPurchased()
    {
        base.OnLandPurchased();

        gameManager.levelData.GoToNextLevel();
    }

    public override void OnLandNotSold()
    {
        base.OnLandNotSold();

        gameManager.levelData.GoToNextLevel();
    }

    public override void OnLandSold()
    {
        base.OnLandSold();

        gameManager.levelData.GoToNextLevel();
    }

    public override void OnDayEnded()
    {
        base.OnDayEnded();

    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public TransformAnimatorAsset   appearAnimator;

    [Space(5.0f)]
    public TextMeshProUGUI          textForShowingDayCounter;
    public TextMeshProUGUI          textForShowingLevelCounter;

    #endregion

    #region Configuretion

    private IEnumerator UpdateRemainingCustomerWithDelay() {

        yield return new WaitForSeconds(0.1f);

        UpdateCustomerCounter();

        StopCoroutine(UpdateRemainingCustomerWithDelay());
    }

    private void UpdateCustomerCounter() {

        textForShowingDayCounter.text = "DAY " + (sharedData.dayIndex + 1);
        textForShowingLevelCounter.text = "LV " + (sharedData.levelIndex + 1);

        //int remainingNumberOfCustomer   = sharedData.remainingNumberOfLandSellerAgent + sharedData.remainingNumberOfLandBuyerAgent;
        //int totalNumberOfCustomer       = sharedData.numberOfLandSellerAgent + sharedData.numberOfLandBuyerAgent;
        //int indexOfCustomerServing      = (totalNumberOfCustomer - remainingNumberOfCustomer);
        //textForShowingLevelCounter.text = "LV " + ((sharedData.levelIndex * totalNumberOfCustomer) + indexOfCustomerServing);
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
