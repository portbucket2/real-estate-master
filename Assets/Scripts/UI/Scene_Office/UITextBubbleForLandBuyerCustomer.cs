﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UITextBubbleForLandBuyerCustomer : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnCustomerArrived()
    {
        if (sharedData.currentCustomerType == CustomerType.Buyer) {

            iconForLandType.sprite  = sharedData.landTypeFromBuyer.iconForEnum;
            textForNameOflandType.text     = sharedData.landTypeFromBuyer.nameOfEnum;
            textForOfferingPriceForTheLand.text = gameManager.GetTheBuyingPriceForBuyer().ToString("F0");

            animatorForTextBubble.SetTrigger("APPEAR");
        }
    }

    public override void OnLandNotSold()
    {
        base.OnLandNotSold();
        animatorForTextBubble.SetTrigger("DISAPPEAR");
    }

    public override void OnLandSold()
    {
        base.OnLandSold();
        animatorForTextBubble.SetTrigger("DISAPPEAR");
    }


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public Animator animatorForTextBubble;

    [Space(2.5f)]
    public Image            iconForLandType;
    public TextMeshProUGUI  textForNameOflandType;
    public TextMeshProUGUI  textForOfferingPriceForTheLand;

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
