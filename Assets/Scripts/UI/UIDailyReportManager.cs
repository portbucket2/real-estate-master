﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using com.faith.core;

public class UIDailyReportManager : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        _transformReferenceForNextDayButton = nextDayButton.transform;

        nextDayButton.onClick.AddListener(() =>
        {
            gameManager.centralAnimatorForUIDisappear.DoAnimate(new List<Transform>() { containerForDailyReport }, () => {

                containerForDailyReport.gameObject.SetActive(false);
                gameManager.ChangeGameState(GameState.DayStarted);
            });
        });
    }


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    public override void OnDayEnded()
    {
        base.OnDayEnded();

        gameManager.levelData.GotToNextDay();

        textForEarning.text = sharedData.totalMoneyEarned.ToString("F0");
        textForExpense.text = sharedData.totalMoneySpent.ToString("F0");
        textForProfit.text = (sharedData.totalMoneyEarned - sharedData.totalMoneySpent).ToString("F0");

        _transformReferenceForNextDayButton.localScale = Vector3.zero;

        containerForDailyReport.gameObject.SetActive(true);
        gameManager.centralAnimatorForUIAppear.DoAnimate(
            new List<Transform>() {
                containerForDailyReport
            },
            () => {
                StartCoroutine(DelayForShowingNextDayButton());
            });
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public Transform containerForDailyReport;

    [Space(5.0f)]
    public TextMeshProUGUI  textForExpense;
    public TextMeshProUGUI  textForEarning;
    public TextMeshProUGUI  textForProfit;

    [Space(2.5f)]
    public Button           nextDayButton;
    public FloatReference   delayForShwoingNextDayButton;

    #endregion

    #region Private Variables

    private Transform _transformReferenceForNextDayButton;

    #endregion

    #region Configuretion

    private IEnumerator DelayForShowingNextDayButton() {

        yield return new WaitForSeconds(delayForShwoingNextDayButton);

        gameManager.centralAnimatorForUIAppear.DoAnimate(new List<Transform>() { _transformReferenceForNextDayButton });

        StopCoroutine(DelayForShowingNextDayButton());
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
