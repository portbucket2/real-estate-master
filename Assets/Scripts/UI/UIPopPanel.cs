﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using com.faith.core;

public class UIPopPanel : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public Transform rootContainer;

    [Space(5.0f)]
    public TextMeshProUGUI textForHeadline;
    public TextMeshProUGUI textForMessage;
    public TextMeshProUGUI textForButtonInfo;

    [Space(5.0f)]
    public Image imageForInteractableButton;

    [Space(5.0f)]
    public Button interactableButton;
    public Button closeButton;

    #endregion

    #region Public Callback

    public void ShowPopUp(PopUpPanelInfo popUpPanelInfo, UnityAction OnInteractableButtonPressed, UnityAction OnPopUpPanelClose = null) {

        ShowPopUp(popUpPanelInfo.headline, popUpPanelInfo.message, popUpPanelInfo.buttonInfo, popUpPanelInfo.iconForInteractableButton, OnInteractableButtonPressed, OnPopUpPanelClose);
    }

    public void ShowPopUp(PopUpPanelInfo popUpPanelInfo, string overrideButtonInfo, UnityAction OnInteractableButtonPressed, UnityAction OnPopUpPanelClose = null)
    {

        ShowPopUp(popUpPanelInfo.headline, popUpPanelInfo.message, overrideButtonInfo, popUpPanelInfo.iconForInteractableButton, OnInteractableButtonPressed, OnPopUpPanelClose);
    }

    public void ShowPopUp(string headline, string message, string buttonText, Sprite buttonIcon, UnityAction OnInteractableButtonPressed, UnityAction OnPopUpPanelClose = null) {

        textForHeadline.text = headline;
        textForMessage.text = message;
        textForButtonInfo.text = buttonText;
        imageForInteractableButton.sprite = buttonIcon;

        interactableButton.onClick.RemoveAllListeners();
        interactableButton.onClick.AddListener(()=> {

            ClosePopUp(OnInteractableButtonPressed);
        });

        closeButton.onClick.RemoveAllListeners();
        closeButton.onClick.AddListener(() =>
        {
            ClosePopUp(OnPopUpPanelClose);
        });

        rootContainer.gameObject.SetActive(true);
        gameManager.centralAnimatorForUIAppear.DoAnimate(new List<Transform>() { rootContainer });
    }

    public void ClosePopUp(UnityAction OnPopUpPanelClose = null) {

        gameManager.centralAnimatorForUIDisappear.DoAnimate(new List<Transform>() { rootContainer }, ()=> {
            rootContainer.gameObject.SetActive(false);
            OnPopUpPanelClose?.Invoke();
        });
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
