﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using com.faith.core;
public class UIDecorationPanel : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public TextMeshProUGUI  textForDecorationName;
    public Image            imageForDecorationIcon;
    public Button           buttonForInteractingWithDecorationItem;

    [Space(5.0f)]
    public TransformAnimatorAsset appearAnimator;
    public TransformAnimatorAsset disappearAnimator;

    #endregion

    #region Public Callback

    public void Initialization(
        EnumAssetForDecorationType decoratioType,
        int decorationTypeIndex,
        int decorationItemIndex,
        string nameForDecorationItem,
        Sprite iconForDecorationItem,
        UnityAction<EnumAssetForDecorationType,int, int> OnButtonPressed) {

        textForDecorationName.text = nameForDecorationItem;
        imageForDecorationIcon.sprite = iconForDecorationItem;
        buttonForInteractingWithDecorationItem.onClick.AddListener(() => OnButtonPressed(decoratioType, decorationTypeIndex, decorationItemIndex));

        appearAnimator.DoAnimate(new List<Transform>() { transform });
    }

    public void Disappear() {

        buttonForInteractingWithDecorationItem.interactable = false;
        disappearAnimator.DoAnimate(new List<Transform>() { transform }, delegate
        {
            Destroy(gameObject);
        });
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
