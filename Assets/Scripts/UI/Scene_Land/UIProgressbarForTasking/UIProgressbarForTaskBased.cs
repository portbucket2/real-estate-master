﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using com.faith.core;
public class UIProgressbarForTaskBased : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnHousePlacementStarted()
    {
        base.OnHousePlacementStarted();

        int numberOfHouseTypeAvailable = sharedData.landAssetOnSeller.houseTypeCanBePlaced.Length;
        Initialization(new List<Sprite>() {
            sharedData.landAssetOnSeller.houseTypeCanBePlaced[Random.Range(0, numberOfHouseTypeAvailable)].iconForEnum });
    }

    public override void OnHousePlacementEnded()
    {
        base.OnHousePlacementEnded();

        StartCoroutine(ControllerForDisappearingProgressbar());
    }

    public override void OnDecorationStarted()
    {
        base.OnDecorationStarted();

        int landUniqueID                    = gameManager.inventory.GetLandUniqueID(sharedData.displayTableIndexForDecoringItem, sharedData.displayItemIndexForDecoringItem);
        int numberOfDecorationToBePlaced    = gameManager.landAssets.listOfLandAssetInfo[landUniqueID].decorationType.Length;
        List<Sprite> listOfDecorationIcon   = new List<Sprite>();
        for (int i = 0; i < numberOfDecorationToBePlaced; i++) listOfDecorationIcon.Add(gameManager.landAssets.listOfLandAssetInfo[landUniqueID].decorationType[i].iconForEnum);
        Initialization(listOfDecorationIcon);
    }

    public override void OnDecorationEnded()
    {
        base.OnDecorationEnded();

        StartCoroutine(ControllerForDisappearingProgressbar());
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public GameObject taskIconPrefab;

    [Space(5.0f)]
    public Transform        containerForProgressBar;
    [Range(0.1f,1f)]
    public float            durationForProgressbarToFill = 0f;
    public RectTransform    parentOfProgressbar;
    public Image            progressBarFiller;

    #endregion

    #region Private Variables

    private int _currentProgressionIndex = -1;
    private int _newProgressionIndex = 0;

    private List<UIProgrssbarTaskIcon> _listOfUITaskIcon;

    #endregion

    #region Configuretion

    private IEnumerator ControllerForDisappearingProgressbar() {

        yield return new WaitForSeconds(durationForProgressbarToFill * 2f);

        UIDisappear(containerForProgressBar);

        StopCoroutine(ControllerForDisappearingProgressbar());
    }

    private IEnumerator ControllerForTaskProgressionFiller() {

        float numberOfTask  = _listOfUITaskIcon.Count;
        float fillerStart   = _currentProgressionIndex / numberOfTask;
        float fillerEnd     = _newProgressionIndex / numberOfTask;

        float remainingTimeForProgressbarToFill = durationForProgressbarToFill;
        float cycleLength = 0.0167f;
        WaitForSeconds cycleDelay = new WaitForSeconds(cycleLength);

        while (remainingTimeForProgressbarToFill > 0) {

            float progression = 1f - (remainingTimeForProgressbarToFill / durationForProgressbarToFill);
            float fillAmount = Mathf.Lerp(fillerStart, fillerEnd, progression);
            progressBarFiller.fillAmount = fillAmount;

            yield return cycleDelay;
            remainingTimeForProgressbarToFill -= cycleLength;
        }

        progressBarFiller.fillAmount = Mathf.Lerp(fillerStart, fillerEnd, 1);

        _listOfUITaskIcon[_currentProgressionIndex].Disappear(true);

        StopCoroutine(ControllerForTaskProgressionFiller());
    }

    #endregion

    #region Public Callback

    public void Initialization(List<Sprite> taskIcons) {

        _currentProgressionIndex= -1;
        _newProgressionIndex    = 0;
        progressBarFiller.fillAmount = 0;

        _listOfUITaskIcon = new List<UIProgrssbarTaskIcon>();

        Vector2 sizeOfParent    = parentOfProgressbar.sizeDelta;
        Vector2 startingPoint   = new Vector2(-(sizeOfParent.x / 2), -(sizeOfParent.y / 2));

        int numberOfTaskIcon = taskIcons.Count;
        for (int i = 0; i < numberOfTaskIcon; i++) {

            Vector2 modifiedLocalPosition = new Vector2(
                    startingPoint.x + (((i + 1.0f) / numberOfTaskIcon) * sizeOfParent.x),
                    0
                );

            RectTransform rectTransform = Instantiate(taskIconPrefab, parentOfProgressbar).GetComponent<RectTransform>();
            rectTransform.anchoredPosition = modifiedLocalPosition;

            UIProgrssbarTaskIcon newUITaskIcon = rectTransform.GetComponent<UIProgrssbarTaskIcon>();
            newUITaskIcon.Initialization(taskIcons[i]);
            _listOfUITaskIcon.Add(newUITaskIcon);
        }

        UIAppear(containerForProgressBar);
    }

    public void TaskComplete() {

        _currentProgressionIndex++;
        _newProgressionIndex++;

        StartCoroutine(ControllerForTaskProgressionFiller());
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
