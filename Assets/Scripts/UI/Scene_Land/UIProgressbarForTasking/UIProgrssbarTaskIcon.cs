﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using com.faith.core;
public class UIProgrssbarTaskIcon : BaseRealEstateMasterBehaviour
{
    #region Public Variables

    public Image iconField;

    [Space(5.0f)]
    public TransformAnimatorAsset appearAnimator;
    public TransformAnimatorAsset disappearAnimator;

    #endregion

    #region Public Callback

    public void Initialization(Sprite taskIcon) {

        iconField.sprite = taskIcon;
        UIAppear(transform);
    }



    public void Disappear(bool isDestroy) {


        UIDisappear(
            transform,
            () =>
            {
                if (isDestroy) Destroy(gameObject);
            });

    }

    #endregion
}
