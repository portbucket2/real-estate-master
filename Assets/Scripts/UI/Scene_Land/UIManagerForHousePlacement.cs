﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using com.faith.core;
public class UIManagerForHousePlacement : UIBaseManager
{
    #region ALL UNITY FUNCTIONS


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnHousePlacementStarted()
    {
        base.OnHousePlacementStarted();

        _IsDecoratingInstantly = false;

        UIAppear(containerForDecorationItem, () =>
        {
            StartCoroutine(ControllerOnSpawningUIDecorationPanel(
                    gameManager.SharedGameData.landAssetOnSeller.houseTypeCanBePlaced,
                    () => {
                        //Invoke    :   When all the decorationItem is placed;

                        OnHousePlacementEnd();

                    }, true));
        });
    }

    public override void OnDecorationStarted()
    {
        base.OnDecorationStarted();

        if (!_IsDecoratingInstantly)
        {
            ShowEstimatedPriceForLand();
            UIAppear(containerForEstimatedPrice);
        }

        int displayTableIndex = sharedData.displayTableIndexForDecoringItem;
        int displayItemIndex = sharedData.displayItemIndexForDecoringItem;

        if (!_IsDecoratingInstantly)
        {

            UIAppear(containerForDecorationItem);
            int houseTypeIndex = gameManager.inventory.GetHouseTypeIndex(displayTableIndex, displayItemIndex);
            int houseIndex = gameManager.inventory.GetHouseTypeIndex(displayTableIndex, displayItemIndex);
            environmentPlacementManager.PlaceDecorationItemInEnvironment(environmentPlacementManager.decorationPlacement[0].decorationType[houseTypeIndex], houseIndex, () =>
            {
                CoreDebugger.Debug.Log("HousePlaced For Decoration");
            });
        }

        int landIndex       = gameManager.inventory.GetLandUniqueID(displayTableIndex, displayItemIndex);

        StartCoroutine(ControllerOnSpawningUIDecorationPanel(
            gameManager.landAssets.listOfLandAssetInfo[landIndex].decorationType,
            () => {
                //Invoke    :   When all the decorationItem is placed;

                OnDecorationPlacementEnd(sharedData.displayTableIndexForDecoringItem, sharedData.displayItemIndexForDecoringItem);

            }, false));


    }


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    [Header("Parameter  :   ExternalReference")]
    public SceneReference               officeScene;
    public EnvironmentPlacementManager  environmentPlacementManager;
    public UIProgressbarForTaskBased    taskBarProgression;

    [Header("Parameter  :   UI")]
    public Transform        containerForDecorationItem;
    public RectTransform    parentForDecorationItem;
    public GameObject       prefabForDecorationPanel;

    [Space(5.0f)]
    public FloatReference   intervalOnSpawningDecorationPanel;
    public FloatReference   delayBeforeSwitchingToOfficeScene;

    [Header("Parameter  :   UI For EstimatedValue")]
    public Transform containerForEstimatedPrice;
    public TextMeshProUGUI textForEsimatedValue;

    [Header("Parameter  :   UI For Decoration")]
    public Transform        containerForPostHousePlacementPanel;
    public Button           buttonForHeadingBackToOffice;
    public Button           buttonForDecoration;

    [Space(5.0f)]
    [Header("Parameter  :   Particle/VisualFX")]
    public ParticleSystem confetiParticleForCelebretion;
    public ParticleSystem prefabForDecorationParticleForCelebretion;
    #endregion

    #region Private Variables

    private bool _IsButtonPressedForSelectingDecorationType = false;
    private bool _IsDecoratingInstantly = false;

    private List<UIDecorationPanel> _listOfCurrentUIDecorationPanel;

    #endregion

    #region Configuretion

    private void ShowEstimatedPriceForLand() {

        int displayTableIndex = sharedData.displayTableIndexForDecoringItem;
        int displayItemIndex = sharedData.displayItemIndexForDecoringItem;

        int landUniqueID = gameManager.inventory.GetLandUniqueID(displayTableIndex, displayItemIndex);

        textForEsimatedValue.text = "$" + gameManager.GetEstimatedPriceForLand(landUniqueID, displayTableIndex, displayItemIndex);
    }

    private void OnHousePlacementEnd() {

        gameManager.inventory.AddLandInInventory(
            sharedData.landAssetOnSeller.landIndex,
            (displayTableIndex, displayItemIndex) => {

                sharedData.displayTableIndexForDecoringItem = displayTableIndex;
                sharedData.displayItemIndexForDecoringItem = displayItemIndex;

                CoreDebugger.Debug.Log("Added To Database : " + sharedData.displayTableIndexForDecoringItem + "," + sharedData.displayItemIndexForDecoringItem);

                gameManager.inventory.AddHouseIndex(
                    displayTableIndex,
                    displayItemIndex,
                    sharedData.decorationTypeIndexForRecentPurchasedLand,
                    sharedData.decorationItemIndexForRecentPurchasedLand);

                gameManager.ChangeGameState(GameState.HousePlacementEnded);
            });

        

        buttonForHeadingBackToOffice.onClick.RemoveAllListeners();
        buttonForHeadingBackToOffice.onClick.AddListener(() =>
        {
            UIDisappear(containerForPostHousePlacementPanel, () =>
            {
                sharedData.flagForVisitedTheLandForDecoration = false;
                gameManager.LoadScene(
                        officeScene,
                        OnSceneLoaded: () => {
                            gameManager.ChangeGameState(GameState.CustomerArriving);
                        },
                        initialDelayToInvokeOnSceneLoaded: 0f
                );
            });
        });




        buttonForDecoration.onClick.RemoveAllListeners();
        buttonForDecoration.onClick.AddListener(() =>
        {

            UIDisappear(containerForEstimatedPrice);

            UIDisappear(containerForPostHousePlacementPanel, () =>
            {
                UIAppear(containerForDecorationItem, () =>
                {
                    _IsDecoratingInstantly = true;
                    gameManager.ChangeGameState(GameState.DecorationStarted);
                });
                
            });
        });

        UIDisappear(containerForDecorationItem, () =>
        {
            ShowEstimatedPriceForLand();
            UIAppear(containerForEstimatedPrice);


            UIAppear(containerForPostHousePlacementPanel);
        });        
    }

    private void OnDecorationPlacementEnd(int displayTableIndex, int displayItemIndex) {

        UIDisappear(containerForDecorationItem);

        gameManager.inventory.SetHouseAsDecorated(displayTableIndex, displayItemIndex, true);
        gameManager.ChangeGameState(GameState.DecorationEnded);

        UIAppear(containerForEstimatedPrice, () => {
            
            UIAppear(textForEsimatedValue.transform,() => {
                ShowEstimatedPriceForLand();
            });
        });

        

        StartCoroutine(LoadSceneWithDelay(() =>
        {
            sharedData.flagForVisitedTheLandForDecoration = !_IsDecoratingInstantly ? true : false;
            gameManager.LoadScene(
                officeScene,
                OnSceneLoaded: () =>
                {
                    gameManager.ChangeGameState(GameState.CustomerArriving);
                },
                initialDelayToInvokeOnSceneLoaded: 0.1f);
        }));
    }

    private IEnumerator ControllerOnSpawningUIDecorationPanel(EnumAssetForDecorationType[] decorationTypes, UnityAction OnAllDecorationSpawned, bool showAllOptionAtOnce = false) {

        WaitForSeconds cycleDelay = new WaitForSeconds(intervalOnSpawningDecorationPanel);

        WaitUntil waitUntilButtonIsPressed = new WaitUntil(() =>
        {
            if (_IsButtonPressedForSelectingDecorationType)
                return true;

            return false;
        });

        int numberOfDecorationType = decorationTypes.Length;
        _listOfCurrentUIDecorationPanel = new List<UIDecorationPanel>();
        for (int  i = 0; i < numberOfDecorationType; i++) {

            _IsButtonPressedForSelectingDecorationType = false;
            

            int numberOfDecorationItem = decorationTypes[i].decorationItems.Length;
            for (int j = 0; j < numberOfDecorationItem; j++) {

                UIDecorationPanel newUIDecorationPanel = Instantiate(prefabForDecorationPanel, parentForDecorationItem).GetComponent<UIDecorationPanel>();
                newUIDecorationPanel.Initialization(
                    decorationTypes[i],
                    i,
                    j,
                    decorationTypes[i].decorationItems[j].nameOfDecorationItem,
                    decorationTypes[i].decorationItems[j].iconForDecorationItem,
                    OnButtonPressed);


                _listOfCurrentUIDecorationPanel.Add(newUIDecorationPanel);
                yield return cycleDelay;
            }

            
            if(!showAllOptionAtOnce)
                yield return waitUntilButtonIsPressed;
        }

        if (showAllOptionAtOnce)
            yield return waitUntilButtonIsPressed;

        confetiParticleForCelebretion.Play();
        OnAllDecorationSpawned.Invoke();

    }

    private IEnumerator LoadSceneWithDelay(UnityAction EventAfterDelay) {

        yield return new WaitForSeconds(delayBeforeSwitchingToOfficeScene);
        EventAfterDelay.Invoke();
        StopCoroutine(LoadSceneWithDelay(null));
    }

    private void OnButtonPressed(EnumAssetForDecorationType decorationType, int decorationTypeIndex, int decorationItemIndex) {

        while (_listOfCurrentUIDecorationPanel.Count > 0)
        {
            _listOfCurrentUIDecorationPanel[0].Disappear();
            _listOfCurrentUIDecorationPanel.RemoveAt(0);
        }

        sharedData.decorationTypeIndexForRecentPurchasedLand = decorationTypeIndex;
        sharedData.decorationItemIndexForRecentPurchasedLand = decorationItemIndex;

        environmentPlacementManager.PlaceDecorationItemInEnvironment(
            decorationType,
            decorationItemIndex, delegate
            {
                taskBarProgression.TaskComplete();
                _IsButtonPressedForSelectingDecorationType = true;
            });

        

    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
