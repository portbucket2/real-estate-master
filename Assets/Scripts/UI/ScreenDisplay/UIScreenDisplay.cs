﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using com.faith.core;

public class UIScreenDisplay : BaseRealEstateMasterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        containerForResult.localScale = Vector3.zero;
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnDayStarted()
    {
        base.OnDayStarted();
        ShowDefaultScreen();
    }


    public override void OnCustomerArrived()
    {
        base.OnCustomerArrived();
        if(sharedData.currentCustomerType == CustomerType.Seller)
            StartCoroutine(ControllerForAnalysing());
    }

    public override void OnLandNotPurchased()
    {
        base.OnLandNotPurchased();

        HideInfoPanel();
        ShowDefaultScreen();
    }

    public override void OnLandPurchased()
    {
        base.OnLandPurchased();

        HideInfoPanel();
        ShowDefaultScreen();
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public TextMeshProUGUI defaultTextPanel;
    public TextMeshProUGUI analyzingTextPanel;

    [Header("Reference  :   Analyzing")]
    public FloatVariable    durationForAnalyzing;
    public Image            progressbarForAnalyzing;
    public TextMeshProUGUI  progressNumberForAnalyzing;

    [Space(5.0f), Header("Reference :   Result")]
    public Transform containerForResult;
    public Transform landPreviewPosition;
    public Animator landPreviewAnimator;
    [Space(5.0f)]
    public Image locationIcon;
    public TextMeshProUGUI location;
    public TextMeshProUGUI estimatedPrice;

    #endregion

    #region Private Variables

    private GameObject _landDisplayPrefab;

    #endregion

    #region Configuretion

    private void HideInfoPanel() {

        gameManager.centralAnimatorForUIDisappear.DoAnimate(new List<Transform>() { containerForResult },delegate {

            if (_landDisplayPrefab != null) Destroy(_landDisplayPrefab);

            landPreviewAnimator.enabled = false;
        });
    }

    private void ShowDefaultScreen() {

        defaultTextPanel.text = "Real\nEstate\nMaster";
        gameManager.centralAnimatorForUIAppear.DoAnimate(new List<Transform>() { defaultTextPanel.transform });
    }

    private IEnumerator ControllerForAnalysing()
    {

        float progression = 0;
        float remainingTimeForAnalysing = durationForAnalyzing.Value;
        float cycleLength = 0.0167f;
        WaitForSeconds cycleDelay = new WaitForSeconds(cycleLength);

        bool waitForAnalyzerToBeAppear = false;
        WaitUntil waitUntilAnalyzerAppear = new WaitUntil(() =>
        {
            if (waitForAnalyzerToBeAppear)
                return true;

            return false;
        });

        gameManager.centralAnimatorForUIDisappear.DoAnimate(new List<Transform>() { defaultTextPanel.transform });

        analyzingTextPanel.text = "Analyzing..";
        gameManager.centralAnimatorForUIAppear.DoAnimate(new List<Transform>() { analyzingTextPanel.transform, progressbarForAnalyzing.transform }, delegate {

            waitForAnalyzerToBeAppear = true;
        });

        _landDisplayPrefab = Instantiate(sharedData.landAssetOnSeller.landDisplayPrefab, landPreviewPosition);
        _landDisplayPrefab.transform.localPosition = Vector3.zero;
        landPreviewAnimator.enabled = true;

        yield return waitUntilAnalyzerAppear;

        while (remainingTimeForAnalysing > 0)
        {

            progression = 1f - (remainingTimeForAnalysing / durationForAnalyzing.Value);
            progressbarForAnalyzing.fillAmount = progression;
            progressNumberForAnalyzing.text = ((int)(progression * 100f)) + "%";
            yield return cycleDelay;
            remainingTimeForAnalysing -= cycleLength;
        }

        progressbarForAnalyzing.fillAmount  = 1;
        progressNumberForAnalyzing.text     = "100%";

        gameManager.centralAnimatorForUIDisappear.DoAnimate(new List<Transform>() { analyzingTextPanel.transform, progressbarForAnalyzing.transform }, delegate {

            progressbarForAnalyzing.fillAmount = 0;
            progressNumberForAnalyzing.text = "";
            defaultTextPanel.text = "";
            analyzingTextPanel.text = "";
        });


        locationIcon.sprite = sharedData.landAssetOnSeller.landType.iconForEnum;
        location.text = sharedData.landAssetOnSeller.landType.nameOfEnum;
        estimatedPrice.text = "$" + sharedData.landAssetOnSeller.estimatedPrice;
        gameManager.centralAnimatorForUIAppear.DoAnimate(new List<Transform>() { containerForResult }, delegate {

            gameManager.ChangeGameState(GameState.AnalyzingComplete);
        });

        StopCoroutine(ControllerForAnalysing());
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
