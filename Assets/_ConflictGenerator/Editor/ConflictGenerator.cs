﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class ConflictGenerator : Editor
{
    [UnityEditor.MenuItem("Potato/Generate Conflict")]
    public static void GenerateConflict()
    {
        string path = "ConflictForGreaterGood.txt";

        string data = string.Format("GitConflictGeneration: {0}\n{1} - Device Name: {2}\n{3}\n{4}", System.DateTime.Now, System.DateTime.Now.Ticks, SystemInfo.deviceName, SystemInfo.deviceModel, SystemInfo.deviceUniqueIdentifier);
        StreamWriter sw = File.CreateText(path);
        sw.WriteLine(data);
        sw.Close();
        Debug.LogErrorFormat(data);
    }
}
